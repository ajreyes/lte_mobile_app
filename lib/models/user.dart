import 'package:flutter/material.dart';

// Class for Authenticaded Users 
class User {
  // final String id;
  final String username;
  final String email;
  final int token;

  User({
    // @required this.id,
    @required this.username,
     @required this.email,
    @required this.token,
  });
}