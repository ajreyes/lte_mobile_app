import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class DetalleRecolecta extends StatefulWidget {
  final String recolecta_id;
  DetalleRecolecta({Key key, this.recolecta_id}) : super(key: key);
  @override
  createState() => new DetalleRecolectaState();
}

class Recolecta {
  final int pik_id;
  final int pik_cantidad_paquetes;
  final String cp_nombre;
  final String ru_codigo;
  final int recolectados;
  final bool fch_inicio;
  final bool fch_final;

  Recolecta(this.pik_id, this.pik_cantidad_paquetes, this.cp_nombre,
      this.ru_codigo, this.recolectados, this.fch_inicio, this.fch_final);
}

class DetalleRecolectaState extends State<DetalleRecolecta> {
  TextEditingController _controller;
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  Map<String, dynamic> responseData;
  String message = 'Ha ocurrido un error.';

  DetalleRecolectaState() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneRecolecta(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    _controller = TextEditingController();
  }

  Future recolectaPaquete(paquete, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;

    String authData = "estado_p=RE&paquete_p=$paquete";
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/ActualizaEstadoPaquete/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

  // Metodo para obtener informacion por WS
  Future<List<Recolecta>> obtieneRecolecta(context) async {
    // Variables
    final String recolecta_id = ModalRoute.of(context).settings.arguments;
    //int despachoInt = int.parse(despacho_id);

    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    // _token = prefs.getInt('token');
    http.Response response;
    bool hasError = true;
    List<Recolecta> ListaRecolectas = [];
    try {
      response = await http.get(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ObtieneRecolecta/" +
              recolecta_id);

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);

        var responseData2 = responseData["items"];

        for (var i = 0; i <= responseData2.length - 1; i++) {
          var iniciado = false;
          if (responseData2[i]["pik_fch_inicio"] != null) {
            iniciado = true;
          }
          var finalizado = false;
          if (responseData2[i]["pik_fch_final"] != null) {
            finalizado = true;
          }
          Recolecta despacho = Recolecta(
              responseData2[i]["pik_id"],
              responseData2[i]["pik_cantidad_paquetes"],
              responseData2[i]["cp_nombre"],
              responseData2[i]["ru_codigo"],
              responseData2[i]["recolectados"],
              iniciado,
              finalizado);
          ListaRecolectas.add(despacho);
        }
        setState(() {
          
        });
      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        throw Exception('Fallo al consultar WS');
      }
      return ListaRecolectas;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaRecolectas;
  }

  Future<void> IniciarRecolecta(String campo) async {
    final String recolecta_id = ModalRoute.of(context).settings.arguments;
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;

    String authData = "recolecta_id=$recolecta_id&campo=$campo";
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/ActualizaFchRecolecta/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return null;
  }

  Widget InputText() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            obscureText: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Número de Paquete',
            ),
            controller: _controller,
            onSubmitted: (term) {
              recolectaPaquete(term, context);
            },
            focusNode: myFocusNode,
            //autofocus: true,
          ),
        )
      ],
    );
  }

  Widget BotonIniciar() {
    return Container(
      margin: const EdgeInsets.only(top: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FlatButton(
            onPressed: () => IniciarRecolecta("I"),
            color: Colors.yellow[500],
            padding: EdgeInsets.all(15.0),
            child: Column(
              // Replace with a Row for horizontal icon + text
              children: <Widget>[Icon(Icons.timer), Text("INICIAR RECOLECTA")],
            ),
          ),
        ],
      ),
    );
  }

    Widget BotonFinalizar() {
    return Container(
      margin: const EdgeInsets.only(top: 30.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          FlatButton(
            onPressed: () => IniciarRecolecta("F"),
            color: Colors.red[500],
            padding: EdgeInsets.all(15.0),
            child: Column(
              // Replace with a Row for horizontal icon + text
              children: <Widget>[Icon(Icons.timer), Text("FINALIZAR RECOLECTA")],
            ),
          ),
        ],
      ),
    );
  }


  // DespachosAsignadosChoferState({Key key, @required this.items});
  Widget build(BuildContext context) {
    final String recolecta_id = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("Recolecta número: " + recolecta_id),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneRecolecta(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        Card(
                          elevation: 8.0,
                          margin: new EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 6.0),
                          color: Colors.grey[300],
                          child: Container(
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(right: 12.0),
                                decoration: new BoxDecoration(
                                    border: new Border(
                                        right: new BorderSide(
                                            width: 1.0, color: Colors.black))),
                                child: Icon(Icons.dashboard,
                                    color: Colors.red, size: 50.0),
                              ),
                              title: Text(
                                  snapshot.data[index].cp_nombre.toString() +
                                      ' - ' +
                                      snapshot.data[index].ru_codigo,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                              subtitle: Text(
                                  'Cantidad de paquetes: ' +
                                      snapshot.data[index].pik_cantidad_paquetes
                                          .toString(),
                                  style: TextStyle(color: Colors.black)),
                              isThreeLine: true,
                              onTap: () {
                                Navigator.of(context).pop(
                                    // With MaterialPageRoute, you can pass data between pages,
                                    // but if you have a more complex app, you will quickly get lost.
                                    /*MaterialPageRoute(
                              builder: (context) => PaquetesDespacho(despacho_id: snapshot.data[index].dsh_id.toString()),
                              settings: RouteSettings(
                                arguments:snapshot.data[index].dsh_id.toString(),
                              )
                            ),*/
                                    );
                              },
                            ),
                          ),
                        ),
                        (snapshot.data[index].fch_inicio && !snapshot.data[index].fch_final)
                            ? InputText()
                            : (!snapshot.data[index].fch_final)?BotonIniciar():Text("RECOLECTA FINALIZADA"),
                        (snapshot.data[index].fch_inicio && !snapshot.data[index].fch_final)
                            ? Text(
                                'Pistoleados:',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            : Text(''),
                        (snapshot.data[index].fch_inicio && !snapshot.data[index].fch_final)
                            ? Text(
                                snapshot.data[index].recolectados.toString(),
                                style: TextStyle(
                                    fontSize: 80, color: Colors.blueAccent),
                              )
                            : Text(''),
                          (snapshot.data[index].fch_inicio && !snapshot.data[index].fch_final)
                            ? BotonFinalizar()
                            : Text(''),
                      ],
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
