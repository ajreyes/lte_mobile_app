import 'package:flutter/material.dart';
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as ph;

class RenuevaBd extends StatefulWidget {
  @override
  createState() => new RenuevaBdState();
}

class RenuevaBdState extends State<RenuevaBd> {
  // Variables
  TextEditingController _password;

  void initState() {
    super.initState();
  }

  // avanceDiarioState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("RENUEVA BD"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        padding: EdgeInsets.all(20),
        child: TextField(
          decoration: InputDecoration(
              labelText: 'CONTRASEÑA ',
              labelStyle: TextStyle(
                  fontFamily: 'Montserrat',
                  fontWeight: FontWeight.bold,
                  color: Colors.grey),
              focusedBorder: UnderlineInputBorder(
                  borderSide: BorderSide(color: Colors.green))),
          obscureText: false,
          controller: _password,
          onSubmitted: (value) {
            if(value == "LTE2020BD") {
              _renuevaBD();
            }
          },
        ),
      ),
    );
  }

  void _renuevaBD() async {
    /*PARA ELIMINAR LA BD CUANDO SE REQUIERA ACTUALIZAR*/ 
      var databasesPath = await getDatabasesPath();
      String path = ph.join(await databasesPath, 'entregas_offline.db');
      bool dbexis = await databaseExists(path);
      Database database = await openDatabase(path, version: 1); 
      final Database db = await database;
     if(dbexis) {
         await db.rawQuery("DROP TABLE IF EXISTS Paquetes");
         await db.rawQuery("DROP TABLE IF EXISTS Pagos");
         await db.rawQuery("DROP TABLE IF EXISTS Entrega");
         await db.execute('CREATE TABLE IF NOT EXISTS Paquetes (paquete INTEGER PRIMARY KEY, paqueteIdDis INTEGER, secobra TEXT, montoColones TEXT, montoDolares TEXT, recibe TEXT, telefono TEXT, celular TEXT, requerimiento TEXT, logisticaInversa TEXT, descripcion TEXT, esLte TEXT, tipoCambio DOUBLE)');
         await db.execute('CREATE TABLE IF NOT EXISTS Pagos (id INTEGER PRIMARY KEY AUTOINCREMENT, paqueteID INTEGER, moneda INTEGER, metodo INTEGER, monto DOUBLE, detalle TEXT)');
         await db.execute('CREATE TABLE IF NOT EXISTS Entrega (id INTEGER PRIMARY KEY, numeroPaquete INTEGER, imagenPath TEXT, estado TEXT, sincronizado TEXT, recibidoNombre TEXT, recibidoId TEXT)');
      } 

      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('LISTO'),
            content: Text("LA BASE DE DATOS LOCAL HA SIDO ACTUALIZADA"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        },
      );
  }
}
