import 'dart:convert';
import 'dart:ffi';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as ph;

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class DetalleDespacho extends StatefulWidget {
  final String despacho_id;
  DetalleDespacho({Key key, this.despacho_id}) : super(key: key);
  @override
  createState() => new DetalleDespachoState();
}

class Despacho {
  final int dsh_id;
  final int dsh_ch_id;
  final int ru_id;
  final String ru_codigo;
  final int cantidadpaq;
  final int cantidadpaqpistoleado;

  Despacho(this.dsh_id, this.dsh_ch_id, this.ru_id, this.ru_codigo,
      this.cantidadpaq, this.cantidadpaqpistoleado);
}

class Paquete {
  int paquete;
  int paqueteIdDis;
  String secobra;
  String montoColones;
  String montoDolares;
  String recibe;
  String telefono;
  String celular;
  String requerimiento;
  String logisticaInversa;
  String descripcion;
  String esLte;
  double tipoCambio;

  Paquete(this.paquete, this.paqueteIdDis, this.secobra, this.montoColones, this.montoDolares,
      this.recibe, this.telefono, this.celular, this.requerimiento, this.logisticaInversa, this.descripcion, this.esLte, this.tipoCambio);
  Map<String, dynamic> toMap() {
    return {
      'paquete': paquete,
      'paqueteIdDis': paqueteIdDis,
      'secobra': secobra,
      'montoColones': montoColones,
      'montoDolares': montoDolares,
      'recibe':recibe,
      'telefono':telefono,
      'celular':celular,
      'requerimiento':requerimiento,
      'logisticaInversa':logisticaInversa,
      'descripcion':descripcion,
      'esLte':esLte,
      'tipoCambio':tipoCambio
    };
  }
}


class DetalleDespachoState extends State<DetalleDespacho> {
  TextEditingController _controller;
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  Map<String, dynamic> responseData;
  String message = 'Ha ocurrido un error.';

  DetalleRecolectaState() {
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneDespacho(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    _controller = TextEditingController();
  }

  Future cargaPaquete(paquete, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    String authData = "estado_p=RU&paquete_p=$paquete";
    String message = 'Ha ocurrido un error.';

    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/ActualizaEstadoPaquete/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
         
        _controller.clear();
        FocusScope.of(context).requestFocus(myFocusNode);
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

  Future<bool> verificaDespacho(paquete, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    final String despacho_id = ModalRoute.of(context).settings.arguments;
    int despachoNum = int.parse(despacho_id);
    int paqueteNum = int.parse(paquete);
    String authData = "paquete_p=$paqueteNum&despacho_id=$despachoNum";
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/CargaPaquetePanel/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
          responseData = json.decode(response.body);
          var error_p = responseData["ERROR_P"];
          if(error_p == "F") {
            return false;
          }
        
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return true;
  }

  Future sacarPaqueteDespacho(paquete, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;

    String authData = "paquete_p=$paquete";
    String message = 'Ha ocurrido un error';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/SacarDespacho/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
       Navigator.of(context).pop();
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

Future esLTE(paquete, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    Paquete paqueteEnProceso = Paquete(0, 0, "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",500);
    String authData = "paquete_p=$paquete";
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/EsLte/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        if(await verificaDespacho(paquete, context)) {
          var esLTE = responseData["LTE_P"];
          if(esLTE == "S") {
            paqueteEnProceso = await obtieneDataLTE(paquete, context, responseData["PAQUETE_ID"]);
            await actualizaEstadoLTE("17", paquete);
          }else {
            paqueteEnProceso = Paquete(int.parse(paquete),0, "N", "0", "0", "-", "-", "-", "-", "-", "-", "-",500);
            paqueteEnProceso.recibe = responseData["CLIENTE_FINAL"];
            paqueteEnProceso.telefono = responseData["TELEFONO"].toString();
            paqueteEnProceso.tipoCambio = 500;
          }
            paqueteEnProceso.paqueteIdDis = responseData["PAQUETE_ID"];
            paqueteEnProceso.descripcion = responseData["DESCRIPCION"];
            paqueteEnProceso.logisticaInversa = responseData["LOGISTICA_INVERSA"];
            paqueteEnProceso.celular = responseData["CELULAR"].toString();
            paqueteEnProceso.requerimiento = responseData["REQ_ESPECIAL"].toString();
            paqueteEnProceso.esLte = esLTE;

          if(paqueteEnProceso.secobra != "SIN FACTURA") {
            await insertarPaquete(paqueteEnProceso);
            await cargaPaquete(paquete, context);
          }

        }else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('ERROR CON PAQUETE'),
                content: Text("EL PAQUETE NO PERTENECE A ESTE DESPACHO"),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );

        }
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

  Future<Paquete> obtieneDataLTE(paquete, context, idDis) async {
    http.Response response;
    bool hasError = true;
    Paquete paqueteFlag = Paquete(0, 0, "-", "-", "-", "-", "-", "-", "-", "-", "-", "-", 500); //en caso de error se retorna este paquete
    //Map<String, dynamic> responseData;
    String ppaquete = paquete.toString();
    String pusuario = "LTEMIAMI";
    String pclave = "RtXlN8cXOxSMy8YI4gL8qNuE3_5Yp4kUm4wYQmKFgiq5wPzdSV";
    //String authData = "pusuario=$pusuario&pclave=$pclave&ppaquete=$ppaquete";
    var body = json.encode({"pusuario": pusuario, "pclave": pclave, "ppaquete": ppaquete});
    Map<String,String> headers = {
      'Content-type' : 'application/json', 
      'Accept': 'application/json',
    };
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://200.74.255.35:8080/ords/xpe/miami/getpaquetepararuta',
            body: body,
            headers: headers
            //encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        
        var responseLTE = json.decode(response.body);
        //var responseResultado = responseLTE["RESULTADO"];
        if(responseLTE["RESULTADO"]["ERROR"] == "S") {
          paqueteFlag.secobra = "SIN FACTURA";
           showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('SIN FACTURA'),
                content: Text(responseLTE["RESULTADO"]["MENSAJE"]),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  FlatButton(
                    child: Text('SACAR DEL DESPACHO'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      sacarPaqueteDespacho(idDis, context);
                    },
                  )
                ],
              );
            },
          );
        }else {
          var paqueteData = responseLTE["RESULTADO"]["PAQUETE"][0];
          
          int paqueteId = paqueteData["XPQ_PAQUETE"];
          String paqueteSecobra =  paqueteData["XFA_SE_COBRA"];
          String colones = paqueteData["XFA_MONTO_COLONES"].toString();
          String dolares = paqueteData["XFA_MONTO_DOLARES"].toString();
          String razonSocial = paqueteData["XCL_RAZON_SOCIAL"];
          String telefono = paqueteData["XCL_TEL_MOVIL"].toString();
          String tipoCambio = paqueteData["XFA_TIPO_CAMBIO"].toString();
          double tipoC = double.parse(tipoCambio);
          Paquete paqueteIns = Paquete(paqueteId, 0, paqueteSecobra, colones, dolares, razonSocial, telefono, "-", "-", "-", "-", "-", tipoC);

          return paqueteIns;
          //insertarPaquete(paqueteIns);
          //cargaPaquete(paquete, context);
        }
      } else {
        throw Exception('Fallo al consultar WS');

      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return paqueteFlag;
  }
  
Future<bool> actualizaEstadoLTE(String estado, String paquete) async {
    http.Response response;
    String pusuario = "LTEMIAMI";
    String pclave = "RtXlN8cXOxSMy8YI4gL8qNuE3_5Yp4kUm4wYQmKFgiq5wPzdSV";
     Map<String,String> headers = {
      'Content-type' : 'application/json', 
      'Accept': 'application/json',
    };
    //String authData = "pusuario=$pusuario&pclave=$pclave&ppaquete=$ppaquete";
    var body = json.encode({"pusuario": pusuario, "pclave": pclave, "ppaquete": paquete, "pestado":estado});
    try {
      response = await http
          .post(
            'http://200.74.255.35:8080/ords/xpe/miami/cambiaestadopaquete/',
            body: body,
            headers: headers
            //encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        var responseLTE = json.decode(response.body);
        //var responseResultado = responseLTE["RESULTADO"];
        if(responseLTE["RESULTADO"]["ERROR"] == "S") {
          
        }
      } else {
        throw Exception('Fallo al consultar WS');

      }
    } catch (error) {
      return false;
    }
    return false;
  }

  Future insertarPaquete(Paquete paquete) async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    bool dbexis = await databaseExists(path);
    Database database = await openDatabase(path, version: 1,
        onCreate: (Database db, int version) async {}
    ); 
    final Database db = await database;
    if(dbexis) {
      await db.execute(
          'CREATE TABLE IF NOT EXISTS Paquetes (paquete INTEGER PRIMARY KEY, paqueteIdDis INTEGER, secobra TEXT, montoColones TEXT, montoDolares TEXT, recibe TEXT, telefono TEXT, celular TEXT, requerimiento TEXT, logisticaInversa TEXT, descripcion TEXT, esLte TEXT, tipoCambio DOUBLE)');
    }
    //await deleteDatabase(path);
    await db.insert(
      'Paquetes',
      paquete.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );
  }
  
  // Metodo para obtener informacion por WS
  Future<List<Despacho>> obtieneDespacho(context) async {
    // Variables
    final String despacho_id = ModalRoute.of(context).settings.arguments;
    //int despachoInt = int.parse(despacho_id);

    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    // _token = prefs.getInt('token');
    http.Response response;
    bool hasError = true;
    List<Despacho> ListaRecolectas = [];
    try {
      response = await http.get(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ObtieneDespacho/" +
              despacho_id).timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);

        var responseData2 = responseData["items"];

        for (var i = 0; i <= responseData2.length - 1; i++) {
          Despacho despacho = Despacho(
              responseData2[i]["dsh_id"],
              responseData2[i]["dsh_ch_id"],
              responseData2[i]["ru_id"],
              responseData2[i]["ru_codigo"],
              responseData2[i]["cantidadpaq"],
              responseData2[i]["cantidadpaqpistoleado"]);
          ListaRecolectas.add(despacho);
        }
        setState(() {});
      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        throw Exception('Fallo al consultar WS');
      }
      return ListaRecolectas;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaRecolectas;
  }

  Widget InputText() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: TextField(
            obscureText: false,
            decoration: InputDecoration(
              border: OutlineInputBorder(),
              labelText: 'Número de Paquete',
            ),
            controller: _controller,
            onSubmitted: (term) {
             // cargaPaquete(term, context);
               esLTE(term, context);
            },
            focusNode: myFocusNode,
            //autofocus: true,
          ),
        )
      ],
    );
  }

  // DespachosAsignadosChoferState({Key key, @required this.items});
  Widget build(BuildContext context) {
    final String despacho_id = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      appBar: AppBar(
        title: Text("Despacho número: " + despacho_id),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneDespacho(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: <Widget>[
                        Card(
                          elevation: 8.0,
                          margin: new EdgeInsets.symmetric(
                              horizontal: 10.0, vertical: 6.0),
                          color: Colors.grey[300],
                          child: Container(
                            child: ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(right: 12.0),
                                decoration: new BoxDecoration(
                                    border: new Border(
                                        right: new BorderSide(
                                            width: 1.0, color: Colors.black))),
                                child: Icon(Icons.dashboard,
                                    color: Colors.red, size: 50.0),
                              ),
                              title: Text(
                                  snapshot.data[index].dsh_id.toString() +
                                      ' - ' +
                                      snapshot.data[index].ru_codigo,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                              subtitle: Text(
                                  'Cantidad de paquetes: ' +
                                      snapshot.data[index].cantidadpaq
                                          .toString(),
                                  style: TextStyle(color: Colors.black)),
                              isThreeLine: true,
                              onTap: () {
                                Navigator.of(context).pop(
                                    // With MaterialPageRoute, you can pass data between pages,
                                    // but if you have a more complex app, you will quickly get lost.
                                    /*MaterialPageRoute(
                              builder: (context) => PaquetesDespacho(despacho_id: snapshot.data[index].dsh_id.toString()),
                              settings: RouteSettings(
                                arguments:snapshot.data[index].dsh_id.toString(),
                              )
                            ),*/
                                    );
                              },
                            ),
                          ),
                        ),
                        (snapshot.data[index].cantidadpaq >
                                snapshot.data[index].cantidadpaqpistoleado)
                            ? InputText()
                            : Text("CARGA COMPLETADA"),
                        (snapshot.data[index].cantidadpaq >
                                snapshot.data[index].cantidadpaqpistoleado)
                            ? Text(
                                'Pistoleados:',
                                style: TextStyle(
                                    fontSize: 20,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold),
                              )
                            : Text(""),
                        (snapshot.data[index].cantidadpaq >
                                snapshot.data[index].cantidadpaqpistoleado)
                            ? Text(
                                snapshot.data[index].cantidadpaqpistoleado
                                    .toString(),
                                style: TextStyle(
                                    fontSize: 80, color: Colors.blueAccent),
                              )
                            : Text(''),
                      ],
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
