import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class avanceDiario extends StatefulWidget {
  @override
  createState() => new avanceDiarioState();
}

class ReporteCierreRuta {
 String criterio;
 double detalle;
 ReporteCierreRuta(
    this.criterio, 
    this.detalle,
  );
}

class avanceDiarioState extends State<avanceDiario> {
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  int _token = 0;
  Map<String, dynamic> responseData;
  String message = 'Ha ocurrido un error.';

  avanceDiarioState() {
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneDespachos(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }


    ReporteCierreRuta iterateMapEntry(String key, dynamic value) {
       double valor = double.parse(value.toString());
       key = key.replaceAll("_", " ");
       key = key.substring(0, key.length - 1);
        ReporteCierreRuta reporte = new ReporteCierreRuta(
          key, 
          valor
        );
        return reporte;
    }
  // Metodo para obtener informacion por WS
  Future<List<ReporteCierreRuta>> obtieneDespachos(context) async {
    // Variables
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getInt('token');
     String authData = "CHOFER_P=$_token";
    http.Response response;
    List<ReporteCierreRuta> ListaReporte = [];
    
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/DetalleAvanceEntrega/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );
      

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        
        responseData.forEach((k, v) {
          ListaReporte.add(iterateMapEntry(k,v));
        });

      } else {
         ListaReporte.add(iterateMapEntry("NO HAY RUTAS EN PROCESO",0)); 
      }
       ListaReporte.sort((a, b) => a.criterio.compareTo(b.criterio));
      return ListaReporte;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      //responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    ListaReporte.sort((a, b) => a.criterio.compareTo(b.criterio));
    return ListaReporte;
  }

  // avanceDiarioState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reporte de Cierre"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneDespachos(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        elevation: 1.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 5.0),
                        color: Colors.grey[300],
                        child: Column(
                          children: <Widget>[
                            ListTile(
                            title: Text(
                                snapshot.data[index].criterio.toString()+": ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black)
                            ),
                            trailing:Text(
                                snapshot.data[index].detalle.toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 20)
                            ),
                          ),
                          ],
                        ));
                  });
            }
          },
        ),
      ),
    );
  }
}
