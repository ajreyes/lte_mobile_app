import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:paqueteslte/detalleRecolecta.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class RecolectasAsignadasChofer extends StatefulWidget {
  @override
  createState() => new RecolectasAsignadasChoferState();
}

class Recolecta {
  final int pik_id;
  final int pik_cantidad_paquetes;
  final String cp_nombre;
  final String ru_codigo;

  Recolecta(
      this.pik_id, this.pik_cantidad_paquetes, this.cp_nombre, this.ru_codigo);
}

class RecolectasAsignadasChoferState extends State<RecolectasAsignadasChofer> {
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  int _token = 0;
  Map<String, dynamic> responseData;
  // var responseData;
  String message = 'Ha ocurrido un error.';

  RecolectasAsignadasChoferState() {
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneRecolectas(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    // WidgetsBinding.instance.addPostFrameCallback((_) => obtieneDespachos(context));
  }

  // Metodo para obtener informacion por WS
  Future<List<Recolecta>> obtieneRecolectas(context) async {
    // Variables
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getInt('token');
    http.Response response;
    bool hasError = true;
    List<Recolecta> ListaRecolectas = [];
    try {
      response = await http.get(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ObtieneRecolectas/" +
              _token.toString() +
              "");

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);

        var responseData2 = responseData["items"];

        for (var i = 0; i <= responseData2.length - 1; i++) {
          Recolecta despacho = Recolecta(
              responseData2[i]["pik_id"],
              responseData2[i]["pik_cantidad_paquetes"],
              responseData2[i]["cp_nombre"],
              responseData2[i]["ru_codigo"]);
          ListaRecolectas.add(despacho);
        }
      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        throw Exception('Fallo al consultar WS');
      }
      return ListaRecolectas;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaRecolectas;
  }

  // DespachosAsignadosChoferState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mis Recolectas"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneRecolectas(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      elevation: 8.0,
                      margin: new EdgeInsets.symmetric(
                          horizontal: 10.0, vertical: 6.0),
                      color: Colors.grey[300],
                      child: Container(
                        child: ListTile(
                          leading: Container(
                            padding: EdgeInsets.only(right: 12.0),
                            decoration: new BoxDecoration(
                                border: new Border(
                                    right: new BorderSide(
                                        width: 1.0, color: Colors.black))),
                            child: Icon(Icons.dashboard, color: Colors.red, size: 50.0),
                          ),
                          title: Text(
                              snapshot.data[index].cp_nombre.toString() +
                                  ' - ' + snapshot.data[index].pik_id.toString() +
                                  ' - ' +
                                  snapshot.data[index].ru_codigo,
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black)),
                          subtitle: Text(
                              'Cantidad de paquetes: ' +
                                  snapshot.data[index].pik_cantidad_paquetes
                                      .toString(),
                              style: TextStyle(color: Colors.black)),
                          isThreeLine: true,
                          onTap: () {
                            Navigator.of(context).push(
                              MaterialPageRoute(
                              builder: (context) => DetalleRecolecta(recolecta_id: snapshot.data[index].pik_id.toString()),
                              settings: RouteSettings(
                                arguments:snapshot.data[index].pik_id.toString(),
                              )
                            ),
                          );
                          },
                        ),
                      ),
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
