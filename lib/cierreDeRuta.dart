import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share/share.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:sqflite/sqflite.dart';
import 'package:path/path.dart' as ph;
import 'PdfPreviewScreen.dart';
import 'dashboard.dart';
import 'package:pdf/pdf.dart';
import 'package:pdf/widgets.dart' as pw;

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class cierreDeRuta extends StatefulWidget {
  @override
  createState() => new cierreDeRutaState();
}

class ReporteCierreRuta {
 String criterio;
 double detalle;
 ReporteCierreRuta(
    this.criterio, 
    this.detalle,
  );
}

class cierreDeRutaState extends State<cierreDeRuta> {
  // Variables
  final pdf = pw.Document();
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  int _token = 0;
  Map<String, dynamic> responseData;
   int _selectedIndex = 0;
  String message = 'Ha ocurrido un error.';

  cierreDeRutaState() {
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneDespachos(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
  }
  void _onItemTapped(int index) {
    if(index == 1) {
      cerrarRuta();
    }else {
      Navigator.of(context).pop();
    }
    setState(() {
      _selectedIndex = index;
    });
  }

    ReporteCierreRuta iterateMapEntry(String key, dynamic value) {
       double valor = double.parse(value.toString());
       key = key.replaceAll("_", " ");
       key = key.replaceFirst("VST", "VISITAS");
       key = key.replaceFirst("PQT", "PAQUETES");
       key = key.replaceFirst("MNT", "MONTO"); 
       key = key.replaceFirst("EFEC ENTREGA", "EFECTIVIDAD ENTREGA");   
        ReporteCierreRuta reporte = new ReporteCierreRuta(
          key, 
          valor
        );
        return reporte;
    }
  // Metodo para obtener informacion por WS
  Future<List<ReporteCierreRuta>> obtieneDespachos(context) async {
    // Variables
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    _token = prefs.getInt('token');
     String authData = "CH_CHOFER=$_token";
    http.Response response;
    List<ReporteCierreRuta> ListaReporte = [];
    
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/CierreDeRuta/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );
      

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);
        String bodyPdf = "";
        responseData.forEach((k, v) {
          ReporteCierreRuta reporte = iterateMapEntry(k,v);
          ListaReporte.add(reporte);
          bodyPdf += reporte.criterio + ": "+ reporte.detalle.toString() +"\n";
        });
        writePDF(bodyPdf);
        await savePdf();
      } else {
         ListaReporte.add(iterateMapEntry("NO HAY RUTAS EN PROCESO",0)); 
      }
      return ListaReporte;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      //responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaReporte;
  }


  writePDF(String bodyPdf) {
    DateTime now = new DateTime.now();
     pdf.addPage(
      pw.MultiPage(
        pageFormat: PdfPageFormat.a5,
        margin: pw.EdgeInsets.all(32),
        build: (pw.Context context){
          return <pw.Widget>  [
            pw.Header(
              level: 0,
              child: pw.Text("CIRRE DE RUTA -" + now.toString())
            ),
            pw.Header(
              level: 2,
              child: pw.Text("CHOFER: " + _token.toString())
            ),
            pw.Paragraph(
              text: bodyPdf
            ),
          ];
        },
      )
    );
  }

  Future savePdf() async{
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    String documentPath = documentDirectory.path;
    File file = File("$documentPath/cierre-de-ruta.pdf");
    file.writeAsBytesSync(pdf.save());
  }
    // Metodo para obtener informacion por WS
  Future<void> cerrarRuta() async {
    // Variables
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getInt('token');
     String authData = "ID_CHOFER=$_token";
    http.Response response;
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/CerrarRuta/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}, 
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );
      

      if (response.statusCode == 200) {
         var databasesPath = await getDatabasesPath();
        String path = ph.join(await databasesPath, 'entregas_offline.db');
        Database database = await openDatabase(path, version: 1); 
        final Database db = await database;
        responseData = json.decode(response.body);
        if(responseData['ERROR_P'] != 'N') { 
            showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('¡NO ES EL MOMENTO!'),
                content: Text('Aún existen paquetes sin procesar, debe entregarlos o regresarlos al CEDI'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
        }else {
          await db.rawQuery("DELETE FROM Paquetes");
          await db.rawQuery("DELETE FROM Pagos");
          await db.rawQuery("DELETE FROM Entrega");
           Directory documentDirectory = await getApplicationDocumentsDirectory();
            String documentPath = documentDirectory.path;
            final RenderBox box = context.findRenderObject();
            await Share.shareFiles(['$documentPath/cierre-de-ruta.pdf'],
            text: 'CIERRE DE RUTA',
            subject: 'CIERRE DE RUTA',
            sharePositionOrigin: box.localToGlobal(Offset.zero) & box.size);
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('¡RUTA CERRADA!'),
                content: Text('Haz completado la ruta con exito'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).push(
                        // With MaterialPageRoute, you can pass data between pages,
                        // but if you have a more complex app, you will quickly get lost.
                        MaterialPageRoute(
                          builder: (context) => DashBoard(),
                        ),
                      );
                    },
                  )
                ],
              );
            },
          );
        }
        
      

      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      //responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

  // cierreDeRutaState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Reporte de Cierre"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneDespachos(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        elevation: 1.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 5.0),
                        color: Colors.grey[300],
                        child: Column(
                          children: <Widget>[
                            ListTile(
                            title: Text(
                                snapshot.data[index].criterio.toString()+": ",
                                style: TextStyle(
                                    fontWeight: FontWeight.w400,
                                    color: Colors.black)
                            ),
                            trailing:Text(
                                snapshot.data[index].detalle.toString(),
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Colors.black,
                                    fontSize: 20)
                            ),
                          ),
                          ],
                        ));
                  });
            }
          },
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Colors.white,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.arrow_back),
                  title: Text('Regresar'),
                  
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.check_circle_outline),
                  title: Text('Cerrar Ruta'),
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.red[800],
              onTap: _onItemTapped,
            ),
    );
  }
}
