import 'package:scoped_model/scoped_model.dart';
import 'package:paqueteslte/scoped_models.dart/connected_models.dart';

// Scoped Model Classes
// Allow you to easily pass a data Model from a parent Widget down to it's descendants. 
// In addition, it also rebuilds all of the children that use the model when the model is updated.
class MainModel extends Model with ConnectedModels, UserModel, UtilityModel {
}