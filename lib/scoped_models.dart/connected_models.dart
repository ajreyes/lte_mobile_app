import 'dart:convert';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:rxdart/rxdart.dart';
import 'package:paqueteslte/models/auth.dart';
import 'package:paqueteslte/models/user.dart';


// Scoped Model Classes
// Allow you to easily pass a data Model from a parent Widget down to it's descendants.
// In addition, it also rebuilds all of the children that use the model when the model is updated.
class ConnectedModels extends Model {
  User _authenticatedUser;
  bool _isLoading = false;
  bool _activateMenu = true;
}

class UserModel extends ConnectedModels {
  // Timer for loggin out the user

  // this will listen and retunr a boolean saying if an user is authenticated or not
  PublishSubject<bool> _userSubject = PublishSubject();

  User get user {
    return _authenticatedUser;
  }

  PublishSubject<bool> get userSubject {
    return _userSubject;
  }

  void autoAuthenticate() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String token = prefs.getString('token');
    final String expiryTimeString = prefs.getString('expiryTime');
    if (token != null) {
      final DateTime now = DateTime.now();
      final parseExpiryTime = DateTime.parse(expiryTimeString);
      if (parseExpiryTime.isBefore(now)) {
        _authenticatedUser = null;
        notifyListeners();
        return;
      }

      final String usuario = prefs.getString('usuario');
      final int token = prefs.getInt('token');
      final String email = prefs.getString('email');
      final int tokenLifespan = parseExpiryTime.difference(now).inSeconds;

      _authenticatedUser = User(token: token, username: usuario, email:email);

      // calling the event that we do are authenticated
      _userSubject.add(true);

      setAuthTimeout(tokenLifespan);
      notifyListeners();
    }
  }

  Future<Map<String, dynamic>> validarCredenciales(usuario, password, [AuthMode mode = AuthMode.Login]) async {
    notifyListeners();
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    String authData = "email_p=$usuario&password_p=$password";
    String message = 'Ha ocurrido un error.';
    try {
      //dev http://192.168.6.180:8080/devrenoir/tsmwww/lte/
      //prod https://app.laterminalexpress.com/laterminalexpress/dis/lte/
      response = await http.post(
        'http://192.168.6.180:8080/devrenoir/tsmwww/lte/login/',
        body: authData,
        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
        encoding: Encoding.getByName("utf-8"),
      ).timeout(
        Duration(seconds: 10),
      );

      if (response.statusCode == 200) {

       final Map<String, dynamic> responseData = json.decode(response.body);
       if (responseData['ERROR_MSG_P'] != null) {
          message = responseData['ERROR_MSG_P'];
          hasError = false;
        }else {
          hasError = false;
          message = 'Autenticación exitosa!'; 
          _authenticatedUser = User(
            email: usuario,
            username: responseData['NOMBRE_P'],
            token: responseData['SESSION_ID_P']
          );
          _userSubject.add(true);
          final SharedPreferences prefs = await SharedPreferences.getInstance();
          prefs.setString('usuario', responseData['NOMBRE_P']);
          prefs.setString('email', usuario);
          prefs.setInt('token', responseData['SESSION_ID_P']);
          return {'success': !hasError, 'message': message, 'SESSION_ID_P':responseData['SESSION_ID_P']};
        }


      } else {
        
        throw Exception('Fallo al consultar WS');
        
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return {'success': hasError, 'message': message};
  }

  void logout() async {
    //clear the authenticated user and the storage
    _authenticatedUser = null;

    // resetting the auto logout time left as pero the user closed the session
    // _authTimer.cancel();

    // setting an event that the user is not authenticated anymore
    _userSubject.add(false);

    //acces the local storage
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    //clear the values from the device
    
    await prefs.remove('email');
    await prefs.remove('token');
    await prefs.remove('usuario');
    await prefs.remove('firmaCliente');
    await prefs.remove('paquete');
  }

  //logout the user after an amount of time
  void setAuthTimeout(int time) {
    // = Timer(Duration(seconds: time), logout);
    // the logout function get executed when the time is over
  }
}

class UtilityModel extends ConnectedModels {
  bool get isLoading {
    return _isLoading;
  }

  bool get activateMenu {
    return _activateMenu;
  }
}
