import 'package:flutter/material.dart';
import 'package:paqueteslte/cierreDeRuta.dart';
import 'package:paqueteslte/main.dart';
import 'package:paqueteslte/login.dart';
import 'package:paqueteslte/dashboard.dart';
import 'package:paqueteslte/listaDespachos.dart';
import 'package:paqueteslte/listaPaquetesDespacho.dart';
import 'package:paqueteslte/entregaOffline.dart';
import 'package:paqueteslte/renuevaBD.dart';
import 'avanceDiario.dart';
import 'detalleDespacho.dart';
import 'detalleRecolecta.dart';


class RouteGenerator {

  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;

    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => SignupPage());
      /*case '/second':
        // Validation of correct data type
        if (args is String) {
          return MaterialPageRoute(
            builder: (_) => SecondPage(
                  data: args,
                ),
          );
     
        }
        return _errorRoute();*/
     
      case '/EntregaPaquete':
          return MaterialPageRoute(builder: (_) => EntregaPaquete(pq_numero: args));
      case '/EntregaPaqueteOffline':
          return MaterialPageRoute(builder: (_) => EntregaPaqueteOffline(pq_numero: args));
      case '/FirmaCliente':
          return MaterialPageRoute(builder: (_) => SignApp());
      case '/dashboard':
          return MaterialPageRoute(builder: (_) => DashBoard());
      case '/DespachosAsignadosChofer':
          return MaterialPageRoute(builder: (_) => DespachosAsignadosChofer());
      case '/listaPaquetesDespacho':
          return MaterialPageRoute(builder: (_) => PaquetesDespacho(despacho_id: args));
      case '/detalleRecolecta':
          return MaterialPageRoute(builder: (_) => DetalleRecolecta(recolecta_id: args));
      case '/detalleDespacho':
          return MaterialPageRoute(builder: (_) => DetalleDespacho(despacho_id: args));
      case '/cierreDeRuta':
          return MaterialPageRoute(builder: (_) => cierreDeRuta());
      case '/avanceDiario':
          return MaterialPageRoute(builder: (_) => avanceDiario());
      case '/SignupPage':
          return MaterialPageRoute(builder: (_) => SignupPage());
      case '/RenuevaBd':
          return MaterialPageRoute(builder: (_) => RenuevaBd());
            // If args is not of the correct type, return an error page.
        // You can also throw an exception while in development.
        
      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

