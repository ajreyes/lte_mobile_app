import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:path/path.dart' as ph;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:sqflite/sqflite.dart';

const directoryName = 'Signature';

class ScreenArguments {
  final String numpaquete;
  ScreenArguments(this.numpaquete);
}

class EntregaPaqueteOffline extends StatefulWidget {
  final String pq_numero;

  EntregaPaqueteOffline({Key key, this.pq_numero}) : super(key: key);
  @override
  createState() => new EntregaPaqueteOfflineState();
}

class Entrega {
  int id;
  int numeroPaquete;
  String imagenPath;
  String estado;
  String sincronizado;
  String recibidoNombre;
  String recibidoId;

  Entrega(this.id, this.numeroPaquete, this.imagenPath, this.estado,
      this.sincronizado, this.recibidoNombre, this.recibidoId);
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'numeroPaquete': numeroPaquete,
      'imagenPath': imagenPath,
      'estado': estado,
      'sincronizado': sincronizado,
      'recibidoNombre': recibidoNombre,
      'recibidoId': recibidoId
    };
  }
}

class Pago {
  int paqueteID;
  int moneda;
  int metodo;
  double monto;
  String detalle;

  Pago(this.paqueteID, this.moneda, this.metodo, this.monto, this.detalle);
  Map<String, dynamic> toMap() {
    return {
      'paqueteID': paqueteID,
      'moneda': moneda,
      'metodo': metodo,
      'monto': monto,
      'detalle': detalle
    };
  }
}

class Documento {
  final int paquete;
  final int bitacora;
  final String base;

  Documento(this.paquete, this.bitacora, this.base);

  Map<String, dynamic> toJson() => {
        'pq_id': paquete,
        'btr_id': bitacora,
        'documento': base,
      };
}

class EntregaPaqueteOfflineState extends State<EntregaPaqueteOffline> {
  TextEditingController _controller;

  int _paqueteId;
  FocusNode myFocusNode;
  final recibe_nombre = TextEditingController();
  final recibe_id = TextEditingController();

  String estadoFoto =
      "-Registra una foto para respaldar el intento de entrega-";
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);


  void estableceNumeroPaquete(numPaquete) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('paqueteEntrega', numPaquete);
  }

 

  // Open the database and store the reference.
// Open the database and store the reference.



  void initState() {
    super.initState();
    _controller = TextEditingController();

    _paqueteId = 0;
    // myFocusNode = FocusNode();
    //_controller.text = ObtieneVariable();
  }


 
  Future guardaImagen(bitacora_id, entrega) async {
    Map<String, dynamic> responseData;
    http.Response response;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String base64Image = "";
    // Image imagen = Image.file(File(entrega.imagenPath));
    File imgFile = File(entrega.imagenPath);
    List<int> imageBytes = await imgFile.readAsBytesSync();
    base64Image = base64Encode(imageBytes);
    /*List<int> stringBytes = UTF8.encode(myString);
    List<int> gzipBytes = new GZipEncoder().encode(stringBytes);
    String compressedString = BASE64.encode(gzipBytes);*/
    //"R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    //String paqueteId = _paqueteId.toString();
    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    Documento docEnviar = new Documento(_paqueteId, bitacora_id, base64Image);
    String json = jsonEncode(docEnviar);

    //String authData = "pq_id=$paqueteId&btr_id=$bitacora_id&documento=$base64Image";
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/GuardaDocumento/',
            body: json,
            headers: {'Content-Type': 'application/json'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );
      if (response.statusCode == 200) {
        //final Map<String, dynamic> responseData = json.decode(response.body);
        prefs.setString('path', null);
      }
    } catch (error) {
      var message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': true, 'message': message});
      print(message);
    }
  }


  // A method that retrieves all the dogs from the dogs table.
  Future<bool> ObtenerEntregas() async {
    // Get a reference to the database.
    bool hayConexion = await hayconexion();
    if (hayConexion) {
      var databasesPath = await getDatabasesPath();
      String path = ph.join(await databasesPath, 'entregas_offline.db');
      Database database = await openDatabase(path, version: 1); // Get a r
      final Database db = await database;
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('¡SINCRONIZANDO!'),
            content: Text(
                "ESTAMOS SINCRONIZANDO LA INFORMACIÓN. PUEDE CONTINUAR SU TRABAJO"),
            actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () async{
                    bool sincronizando = await aunSincronizando();
                    if(!sincronizando) {
                      Navigator.of(context).pop();
                    }
                   
                  },
                )
              ],
          );
        },
      );
      // Query the table for all The Dogs.
      try {
        final List<Map<String, dynamic>> maps = await db.query('Entrega');
       /*await db.rawQuery(
                "UPDATE Entrega SET sincronizado = 'N' WHERE 1");*/
        List.generate(maps.length, (i) async {
          var sincronizado = maps[i]['sincronizado'];
          if (sincronizado == "N") {
            int id = maps[i]['id'];
            Entrega entrega = Entrega(
                maps[i]['id'],
                maps[i]['numeroPaquete'],
                maps[i]['imagenPath'],
                maps[i]['estado'],
                maps[i]['sincronizado'],
                maps[i]['recibidoNombre'],
                maps[i]['recibidoId']);
            await actualizaEstado(entrega);
            await actualizaEstadoLTE(id.toString(), maps[i]['estado']);
            await db.rawQuery(
                "UPDATE Entrega SET sincronizado = 'S' WHERE id = $id");
           
          }
        });
       
       
        return true;
      } catch (error) {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Estamos al día!'),
              content: Text("No hay nada que sincronizar"),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        //await deleteDatabase(path);
        return true;
      }
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('¡AÚN SIN CONEXIÓN!'),
            content: Text("NO SE PUEDE SINCRONIZAR SIN CONEXIÓN"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
      return true;
    }

    //print(listafinal);
  }

  Future<bool> aunSincronizando() async {
     var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1); // Get a r
    final Database db = await database;
    final List<Map<String, dynamic>> maps = await db.query('Entrega');
    bool encontrado = false;
        List.generate(maps.length, (i) async {
          var sincronizado = maps[i]['sincronizado'];
          if (sincronizado == "N") {
           encontrado = true;
          }
        });

    return encontrado;
  }

  Future<bool> actualizaEstadoLTE(String estado, String paquete) async {
    http.Response response;
    String pusuario = "LTEMIAMI";
    String pclave = "RtXlN8cXOxSMy8YI4gL8qNuE3_5Yp4kUm4wYQmKFgiq5wPzdSV";
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
    };
    //String authData = "pusuario=$pusuario&pclave=$pclave&ppaquete=$ppaquete";
    var body = json.encode({
      "pusuario": pusuario,
      "pclave": pclave,
      "ppaquete": paquete,
      "pestado": estado
    });
    try {
      response = await http
          .post('http://200.74.255.35:8080/ords/xpe/miami/cambiaestadopaquete/',
              body: body, headers: headers
              //encoding: Encoding.getByName("utf-8"),
              )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {

        var responseLTE = json.decode(response.body);
        //var responseResultado = responseLTE["RESULTADO"];
        if (responseLTE["RESULTADO"]["ERROR"] == "S") {}
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      return false;
    }
    return false;
  }

  Future<bool> sincronizaPagos(Entrega entrega) async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = await database;
    //final List<Map<String, dynamic>> maps1 = await db.rawQuery('SELECT * FROM Paquetes WHERE paquete='+paquete);
    final List<Map<String, dynamic>> maps = await db.query('Pagos');
    List.generate(maps.length, (i) async {
      if (maps[i]['paqueteID'] == entrega.id) {
        Pago pago = Pago(maps[i]['paqueteID'], maps[i]['moneda'],
            maps[i]['metodo'], maps[i]['monto'], maps[i]['detalle']);
        await insertaPagoBD(pago);
      }
    });
    return true;
  }

  Future<bool> insertaPagoBD(Pago pago) async {
    http.Response response;
    int paqueteID = pago.paqueteID;
    int moneda = pago.moneda;
    double monto = pago.monto;
    int metodo = pago.metodo;
    String concepto = pago.detalle;
    String authData =
        "paquete_id_p=$paqueteID&moneda_p=$moneda&monto_p=$monto&metodo_p=$metodo&concepto_p=$concepto";
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/InsertaPago/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );

      if (response.statusCode == 200) {
        return true;
      }
    } catch (error) {
      return false;
    }
    return true;
  }

  Future<bool> hayconexion() async {
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
      return false;
    }
    return false;
  }

Future actualizaEstado(Entrega entrega) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    String nuevoEstado = entrega.estado;
    String numeroPaquete = entrega.id.toString();
    String recibido_nombre_p = entrega.recibidoNombre.toUpperCase();
    String recibido_id_p = entrega.recibidoId.toUpperCase();
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String authData = "";
    String ulrAPI = "";

    ulrAPI = "http://192.168.6.180:8080/devrenoir/tsmwww/lte/EntregaPaquete/";
    authData =
        "estado_p=$nuevoEstado&paquete_p=$numeroPaquete&recibido_nombre_p=$recibido_nombre_p&recibido_id_p=$recibido_id_p";

    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            ulrAPI,
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        // return responseData['BITACORA_ID'];
        if (entrega.imagenPath != null) {
          await guardaImagen(responseData['BITACORA_ID'], entrega);
        }
        await sincronizaPagos(entrega);

        _controller.clear();
        recibe_nombre.clear();
        recibe_id.clear();
        prefs.setString("paquete", "");
        prefs.setString('path', null);

        /* setState(() {
          _controller = TextEditingController();
          _image = null;
        });*/
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }
// Now, use the method above to retrieve all the dogs.

  Widget PantallaEntrega() {
    return Center(
      child: Column(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.all(0.0),
            child: RaisedButton(
                padding: EdgeInsets.all(40),
                splashColor: Colors.pinkAccent,
                color: Colors.red,
                child: Column( // Replace with a Row for horizontal icon + text
                  children: <Widget>[
                    Icon(Icons.sync),
                    Text("SINCRONIZAR TODO", style:TextStyle(fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20))
                  ],
                ),
                
                onPressed: () {
                  ObtenerEntregas();
               },
              ),
          ),
        ],
      ),
    );
  }

  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return Scaffold(
      appBar: AppBar(
        title: Text("Sincronizar"),
      ),
      key: key,
      drawer: MenuPrincipal(),
      body: PantallaEntrega(),
    );
  }
}





