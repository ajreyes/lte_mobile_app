import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:paqueteslte/listaPaquetesDespacho.dart';

import 'detalleDespacho.dart';

Future<int> obtieneToken() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final int token = prefs.getInt('token');
  return token;
}

class DespachosAsignadosChofer extends StatefulWidget {
  @override
  createState() => new DespachosAsignadosChoferState();
}

class Despacho {
  final int dsh_id;
  final int dsh_ch_id;
  final int ru_id;
  final String ru_codigo;
  final int cantidadpaq;

  Despacho(this.dsh_id, this.dsh_ch_id, this.ru_id, this.ru_codigo,
      this.cantidadpaq);
}

class DespachosAsignadosChoferState extends State<DespachosAsignadosChofer> {
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  int _token = 0;
  Map<String, dynamic> responseData;
  // var responseData;
  String message = 'Ha ocurrido un error.';

  DespachosAsignadosChoferState() {
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtieneDespachos(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    // WidgetsBinding.instance.addPostFrameCallback((_) => obtieneDespachos(context));
  }

  // Metodo para obtener informacion por WS
  Future<List<Despacho>> obtieneDespachos(context) async {
    // Variables
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    _token = prefs.getInt('token');
    http.Response response;
    bool hasError = true;
    List<Despacho> ListaDespachos = [];
    try {
      response = await http.get(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ObtieneDespachos/" +
              _token.toString() +
              "");

      if (response.statusCode == 200) {
        responseData = json.decode(response.body);

        var responseData2 = responseData["items"];

        for (var i = 0; i <= responseData2.length - 1; i++) {
          Despacho despacho = Despacho(
              responseData2[i]["dsh_id"],
              responseData2[i]["dsh_ch_id"],
              responseData2[i]["ru_id"],
              responseData2[i]["ru_codigo"],
              responseData2[i]["cantidadpaq"]);
          ListaDespachos.add(despacho);
        }
      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        throw Exception('Fallo al consultar WS');
      }
      return ListaDespachos;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaDespachos;
  }

  // DespachosAsignadosChoferState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Mis Despachos"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtieneDespachos(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                        elevation: 8.0,
                        margin: new EdgeInsets.symmetric(
                            horizontal: 10.0, vertical: 6.0),
                        color: Colors.grey[300],
                        child: Column(
                          children: <Widget>[
                            ListTile(
                              leading: Container(
                                padding: EdgeInsets.only(right: 12.0),
                                decoration: new BoxDecoration(
                                    border: new Border(
                                        right: new BorderSide(
                                            width: 1.0, color: Colors.black))),
                                child: Icon(Icons.dashboard,
                                    color: Colors.red, size: 50.0),
                              ),
                              title: Text(
                                  snapshot.data[index].dsh_id.toString() +
                                      ' - ' +
                                      snapshot.data[index].ru_codigo,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black)),
                              subtitle: Text(
                                  'Paquetes en el despacho: ' +
                                      snapshot.data[index].cantidadpaq
                                          .toString(),
                                  style: TextStyle(color: Colors.black)),
                              isThreeLine: true,
                              onTap: () {},
                            ),
                            ButtonBar(
                              children: <Widget>[
                                FlatButton(
                                  child: const Text('VER PAQUETES'),
                                  onPressed: () {
                                    Navigator.of(context).push(
                                      // With MaterialPageRoute, you can pass data between pages,
                                      // but if you have a more complex app, you will quickly get lost.
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              PaquetesDespacho(
                                                  despacho_id: snapshot
                                                      .data[index].dsh_id
                                                      .toString()),
                                          settings: RouteSettings(
                                            arguments: snapshot
                                                .data[index].dsh_id
                                                .toString(),
                                          )),
                                    );
                                  },
                                ),
                                FlatButton(
                                  child: const Text('CARGAR PANEL'),
                                  onPressed: () {
                                     Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (context) =>
                                              DetalleDespacho(
                                                  despacho_id: snapshot
                                                      .data[index].dsh_id
                                                      .toString()),
                                          settings: RouteSettings(
                                            arguments: snapshot
                                                .data[index].dsh_id
                                                .toString(),
                                          )),
                                     );
                                    
                                  },
                                ),
                              ],
                            ),
                          ],
                        ));
                  });
            }
          },
        ),
      ),
    );
  }
}
