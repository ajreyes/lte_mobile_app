import 'package:flutter/material.dart';
import 'package:paqueteslte/avanceDiario.dart';
import 'package:paqueteslte/cierreDeRuta.dart';
import 'package:paqueteslte/renuevaBD.dart';
import 'package:paqueteslte/scoped_models.dart/main.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:paqueteslte/main.dart';
import 'package:paqueteslte/dashboard.dart';
import 'package:paqueteslte/listaDespachos.dart';

import 'entregaOffline.dart';
import 'listaRecolectas.dart';
import 'login.dart';

Future<String> obtieneNombre() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  String usuario = prefs.getString('usuario');
  if (usuario == null) {
    usuario = "LTE";
  }
  return usuario;
}

Future<String> obtieneEmail() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  final String email = prefs.getString('email');
  return email;
}

class MenuPrincipal extends StatefulWidget {
  @override
  MenuPrincipalState createState() {
    return new MenuPrincipalState();
  }
  //createState() => new MenuPrincipalState();
}

class MenuPrincipalState extends State<MenuPrincipal> {
  String _nombre = "";
  String _email = "";

  MenuPrincipalState() {
    obtieneNombre().then((val) => setState(() {
          _nombre = val;
        }));
    obtieneEmail().then((val) => setState(() {
          _email = val;
        }));
  }
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        // Important: Remove any padding from the ListView.
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountsDrawerHeader(
            accountName: Text(_nombre),
            accountEmail: Text(_email),
            currentAccountPicture: CircleAvatar(
              backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
                  ? Colors.blue
                  : Colors.white,
              child: Text(
                _nombre[0],
                style: TextStyle(fontSize: 40.0),
              ),
            ),
          ),
          ListTile(
            title: Text('PANEL DE ADMINISTRACIÓN'),
            leading: Icon(
              Icons.view_module,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => DashBoard(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('MIS RECOLECTAS'),
            leading: Icon(
              Icons.local_shipping,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => RecolectasAsignadasChofer(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('MIS DESPACHOS'),
            leading: Icon(
              Icons.airport_shuttle,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => DespachosAsignadosChofer(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('ENTREGAR PAQUETES'),
            leading: Icon(
              Icons.person,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => EntregaPaquete(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('AVANCE DE ENTREGAS'),
            leading: Icon(
              Icons.departure_board,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => avanceDiario(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('CIERRE DE RUTA'),
            leading: Icon(
              Icons.cancel,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => cierreDeRuta(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('SINCRONIZAR INFORMACIÓN'),
            leading: Icon(
              Icons.sync,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => EntregaPaqueteOffline(),
                ),
              );
            },
          ),
          ListTile(
            title: Text('DROP BD'),
            leading: Icon(
              Icons.delete_forever,
              color: Colors.grey,
            ),
            onTap: () {
              Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => RenuevaBd(),
                ),
              );
            },
          ),
          ScopedModel(
            model: MainModel(),
            child: ScopedModelDescendant<MainModel>(
              builder: (BuildContext context, Widget child, MainModel model) {
                return ListTile(
                  title: Text('CERRAR SESIÓN'),
                  leading: Icon(
                    Icons.call_missed_outgoing,
                    color: Colors.grey,
                  ),
                  onTap: () {
                    _logout(model.logout);
                  },
                );
              },
            ),
          )
        ],
      ),
    );
  }

  void _logout(Function logout) {
     logout();
     Navigator.pushReplacementNamed(context, "/SignupPage");
  }
}
