import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:paqueteslte/main.dart';
import 'package:paqueteslte/listaDespachos.dart';

class PaquetesDespacho extends StatefulWidget {
  final String despacho_id;

  PaquetesDespacho({Key key, this.despacho_id}) : super(key: key);

  @override
  createState() => new PaquetesDespachoState();
}

class Paquetes {
  final int pq_numero;
  final String pq_cliente_final;
  final double pq_peso;
  final double pq_monto;
  final String pq_logistica_inversa;
  final String pq_estado;

  Paquetes(this.pq_numero, this.pq_cliente_final, this.pq_peso, this.pq_monto,
      this.pq_logistica_inversa, this.pq_estado);
}

class PaquetesDespachoState extends State<PaquetesDespacho> {
  // Variables
  final List<String> items = ["1", "2", "3", "4", "5"];
  FocusNode myFocusNode;
  Map<String, dynamic> responseData;
  int _selectedIndex = 0;
  int _paquetesEnRuta = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);
  // var responseData;
  String message = 'Ha ocurrido un error.';

  PaquetesDespachoState() {
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    WidgetsBinding.instance
        .addPostFrameCallback((_) => obtienePaquetes(context));
  }

  void initState() {
    super.initState();
    myFocusNode = FocusNode();
    // obtieneToken().then((val) => setState(() {
    //   _token = val;
    // }));
    // WidgetsBinding.instance.addPostFrameCallback((_) => obtieneDespachos(context));
  }

  void completaDespacho() {
    String mensaje = "";
    if(_paquetesEnRuta > 0) {
      mensaje="No se puede completar un despacho con paquetes sin procesar.";
       showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text(mensaje),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
    }else {
       showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('Completar despacho'),
              content: Text('¿Desea marcar el despacho como completado?'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text('Completar'),
                  color: Colors.blueAccent,
                  onPressed: () {
                    cerrarDespacho();
                  },
                ),
              ],
            );
          },
        );
        //cerrarDespacho();
    }
   
  }

Future cerrarDespacho() async{
    final String despacho_id = ModalRoute.of(context).settings.arguments;
    int despachoInt = int.parse(despacho_id);
    http.Response response;
    String authData = "despacho_id=$despachoInt"; 

    try {
      response = await http.post(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ActualizaEstadoDespacho/",
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"), 
        );

      if (response.statusCode == 200) {
        Navigator.of(context).push(
                // With MaterialPageRoute, you can pass data between pages,
                // but if you have a more complex app, you will quickly get lost.
                MaterialPageRoute(
                  builder: (context) => DespachosAsignadosChofer(),
                ),
              );
      }else {
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text("Ha ocurrido un error actualizando el despacho"),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      //responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }

}

  // Metodo para obtener informacion por WS
  Future<List<Paquetes>> obtienePaquetes(context) async {
    final String despacho_id = ModalRoute.of(context).settings.arguments;
    _paquetesEnRuta = 0;
    // Variables
    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    //_token = prefs.getInt('token');
    http.Response response;
    bool hasError = true;
    List<Paquetes> ListaDespachos = [];
    try {
      response = await http.get(
          "http://192.168.6.180:8080/devrenoir/tsmwww/lte/ObtienePaquetesDespacho/" +
              despacho_id + 
              "").timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        responseData = json.decode(utf8.decode(response.bodyBytes));

        var responseData2 = responseData["items"];

        for (var i = 0; i <= responseData2.length - 1; i++) {
          int numeroPaquete = 0;
          if(responseData2[i]["pq_lte"] == "S") {
            numeroPaquete = int.parse(responseData2[i]["pq_lte_guia"]);
          }else {
            numeroPaquete =  responseData2[i]["pq_numero"];
          } 
          double montoPaquete = 0;
          if(responseData2[i]["pq_monto"] != null) {
            montoPaquete = double.parse(responseData2[i]["pq_monto"].toString());
          }
          Paquetes despacho = Paquetes(
              numeroPaquete,
              responseData2[i]["pq_cliente_final"],
              double.parse(responseData2[i]["pq_peso"].toString()),
              montoPaquete,
              responseData2[i]["pq_logistica_inversa"],
              responseData2[i]["pq_estado"]);
          ListaDespachos.add(despacho);
          if(responseData2[i]["pq_estado"] == "RU") {
            _paquetesEnRuta++;
          }
        }
      } else {
        // Error
        showDialog(
          context: context,
          builder: (BuildContext context) {
            return AlertDialog(
              title: Text('¡Error!'),
              content: Text('Fallo al consultar WS'),
              actions: <Widget>[
                FlatButton(
                  child: Text('Ok'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                )
              ],
            );
          },
        );
        throw Exception('Fallo al consultar WS');
      }
      return ListaDespachos;
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error.toString();
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
    return ListaDespachos;
  }


  void _onItemTapped(int index) {
    if(index == 1) {
      completaDespacho();
    }else {
      Navigator.of(context).pop();
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  // DespachosAsignadosChoferState({Key key, @required this.items});
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Paquetes del despacho"),
      ),
      drawer: MenuPrincipal(),
      body: Container(
        child: FutureBuilder(
          future: obtienePaquetes(context),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return Container(
                child: Center(
                  child: CircularProgressIndicator(),
                ),
              );
            } else {
              return ListView.builder(
                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Card(
                      color: (snapshot.data[index].pq_estado != "RU")
                          ? Colors.grey
                          : Colors.grey[300], 
                      child: ListTile(
                        leading: Icon(Icons.mail,
                            color: Colors.red[300], size: 50.0),
                        title: Text(
                            snapshot.data[index].pq_numero.toString() +
                                ' - ' +
                                snapshot.data[index].pq_peso.toString() +
                                " Kg - "+ snapshot.data[index].pq_estado,
                            style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Colors.black)),
                        subtitle: Text(snapshot.data[index].pq_cliente_final,
                            style: TextStyle(color: Colors.black)),
                        isThreeLine: true,
                        onTap: () {
                          /*Navigator.of(context).push(
                            // With MaterialPageRoute, you can pass data between pages,
                            // but if you have a more complex app, you will quickly get lost.
                            MaterialPageRoute(
                                builder: (context) => EntregaPaquete(
                                    pq_numero: snapshot.data[index].pq_numero
                                        .toString()),
                                settings: RouteSettings(
                                  arguments:
                                      snapshot.data[index].pq_numero.toString(),
                                )),
                          );*/
                        },
                      ),
                    );
                    
                  });
                  
            }
          },
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
              backgroundColor: Colors.white,
              items: const <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.arrow_back),
                  title: Text('Regresar'),
                  
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.check_circle_outline),
                  title: Text('Completar Despacho'),
                ),
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.red[800],
              onTap: _onItemTapped,
            ),
    );
  }
}
