import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:paqueteslte/scoped_models.dart/main.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:sqflite/sqflite.dart';
import 'models/auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:path/path.dart' as ph;


class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> with TickerProviderStateMixin {
  TextEditingController _usuario;
  TextEditingController _password;
  AuthMode _authMode = AuthMode.Login;
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  final Firestore _db = Firestore.instance;


  void initState() {
    super.initState();
    _usuario = TextEditingController();
    _password = TextEditingController();
    firebaseCloudMessaging_Listeners();

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.grey[300],
        resizeToAvoidBottomPadding: false,
        body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                child: Stack(
                  children: <Widget>[
                    Container(
                      padding:
                          EdgeInsets.only(top: 125.0, left: 30.0, right: 20.0),
                      child: Image(
                        image: AssetImage('assets/lteLogo.png'),
                        alignment: Alignment.center,
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                  padding: EdgeInsets.only(top: 35.0, left: 20.0, right: 20.0),
                  child: Column(
                    children: <Widget>[
                      TextField(
                        decoration: InputDecoration(
                            labelText: 'EMAIL',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                            // hintText: 'EMAIL',
                            // hintStyle: ,
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.green))),
                        controller: _usuario,
                      ),
                      SizedBox(height: 10.0),
                      TextField(
                        decoration: InputDecoration(
                            labelText: 'CONTRASEÑA ',
                            labelStyle: TextStyle(
                                fontFamily: 'Montserrat',
                                fontWeight: FontWeight.bold,
                                color: Colors.grey),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.green))),
                        obscureText: true,
                        controller: _password,
                      ),
                      SizedBox(height: 40.0),
                      ScopedModel(
                        model: MainModel(),
                        child: ScopedModelDescendant<MainModel>(
                          builder: (BuildContext context, Widget child,
                              MainModel model) {
                            return RaisedButton(
                              padding: EdgeInsets.only(left: 100, right: 100),
                              textColor: Colors.white,
                              color: Colors.red[500],
                              child: Text(
                                'Ingresar',
                                style: TextStyle(fontSize: 22.0),
                              ),
                              //passing just the reference of the login method
                              onPressed: () =>
                                  _submit(model.validarCredenciales),
                            );
                          },
                        ),
                      )
                    ],
                  )),
            ]));
  }

  void _submit(Function validarCredenciales) async {
    var usuario = _usuario.value.text;
    var password = _password.value.text;
    Map<String, dynamic> successInformation;

    successInformation =
        await validarCredenciales(usuario, password, _authMode);

    if (successInformation['success']) {
      firebaseCloudMessaging_Listeners();
      _saveDeviceToken(usuario, successInformation['SESSION_ID_P']);

      /*PARA ELIMINAR LA BD CUANDO SE REQUIERA ACTUALIZAR*/ 
      var databasesPath = await getDatabasesPath();
      String path = ph.join(await databasesPath, 'entregas_offline.db');
      bool dbexis = await databaseExists(path);
      Database database = await openDatabase(path, version: 1); 
      final Database db = await database;
     if(dbexis) {
        /*await db.rawQuery("DROP TABLE Paquetes");*/
        /*await db.rawQuery("DROP TABLE Pagos");
        await db.rawQuery("DROP TABLE Entrega");*/
      } 


      Navigator.pushReplacementNamed(context, '/dashboard');
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Error al ingresar a la aplicación!'),
            content: Text(successInformation['message']),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

void firebaseCloudMessaging_Listeners() {

  _firebaseMessaging.getToken().then((token){
    print(token);
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
  );
}

 _saveDeviceToken(String usuario, int id) async {
    // Get the current user
    // FirebaseUser user = await _auth.currentUser();

    // Get the token for this device
    String fcmToken = await _firebaseMessaging.getToken();

    // Save it to Firestore
    if (fcmToken != null) {
      var tokens = _db
          .collection('users')
          .document(id.toString())
          .collection('tokens')
          .document(fcmToken);

      await tokens.setData({
        'email':usuario,
        'id': id,
        'token': fcmToken,
        
      });
    }
  }

}
