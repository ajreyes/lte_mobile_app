import 'dart:convert';
import 'dart:io';
import 'dart:math';
import 'dart:ui' as ui;
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:paqueteslte/dashboard.dart';
import 'package:paqueteslte/route_generator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter_boom_menu/flutter_boom_menu.dart';
import 'package:sqflite/sqflite.dart';
import 'scoped_models.dart/main.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:easy_permission_validator/easy_permission_validator.dart';
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:path/path.dart' as ph;

const directoryName = 'Signature';

void main() => runApp(MyApp());

class ScreenArguments {
  final String numpaquete;
  ScreenArguments(this.numpaquete);
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }
}

class _MyAppState extends State<MyApp> {
  // This model will handle and allow to pass a data Model from a parent Widget down to it's descendants
  final MainModel _model = MainModel();

  // used with the Subjects events for the user sessions
  bool _isAuthenticated = false;

  // Method called when the App is initiating
  @override
  void initState() {
    // try to authenticate by the token from the device
    //_model.autoAuthenticate();

    // this event will be fired when we are not authenticated anymore
    // and will not be fired at the initState event but when the listen gets fired
    _model.userSubject.listen((bool isAuthenticated) {
      setState(() {
        // setting _isAuthenticated from what is received by the listener
        _isAuthenticated = isAuthenticated;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
          primarySwatch: Colors.red,
          scaffoldBackgroundColor: const Color(0xFFEFEFEF)),
      // Initially display FirstPage
      initialRoute: !_isAuthenticated ? '/' : DashBoard(),
      onGenerateRoute: RouteGenerator.generateRoute,
    );
  }
}

// *  *  *  Entrega Paquete  *  *  *
class EntregaPaquete extends StatefulWidget {
  final String pq_numero;

  EntregaPaquete({Key key, this.pq_numero}) : super(key: key);
  @override
  createState() => new EntregaPaqueteState();
}

class Documento {
  final int paquete;
  final int bitacora;
  final String base;

  Documento(this.paquete, this.bitacora, this.base);

  Map<String, dynamic> toJson() => {
        'pq_id': paquete,
        'btr_id': bitacora,
        'documento': base,
      };
}

class Entrega {
   int id;
   int numeroPaquete;
   String imagenPath;
   String estado;
   String sincronizado;
   String recibidoNombre;
   String recibidoId;

  Entrega(this.id, this.numeroPaquete, this.imagenPath, this.estado,
      this.sincronizado, this.recibidoNombre, this.recibidoId);
  Map<String, dynamic> toMap() {
    return {
      'id': id,
      'numeroPaquete': numeroPaquete,
      'imagenPath': imagenPath,
      'estado': estado,
      'sincronizado': sincronizado,
      'recibidoNombre':recibidoNombre,
      'recibidoId':recibidoId
    };
  }
}

class Pago {
  int paqueteID;
  int moneda;
  int metodo;
  double monto;
  String detalle;

  Pago(
    this.paqueteID,
    this.moneda,
    this.metodo,
    this.monto,
    this.detalle
  );
    Map<String, dynamic> toMap() {
    return {
      'paqueteID': paqueteID,
      'moneda': moneda,
      'metodo': metodo,
      'monto': monto,
      'detalle': detalle
    };
  }
}

class Paquete {
  int paquete;
  int paqueteIdDis;
  String secobra;
  String montoColones;
  String montoDolares;
  String recibe;
  String telefono;
  String celular;
  String requerimiento;
  String logisticaInversa;
  String descripcion;
  String esLte;
  double tipoCambio;

  Paquete(
      this.paquete,
      this.paqueteIdDis,
      this.secobra,
      this.montoColones,
      this.montoDolares,
      this.recibe,
      this.telefono,
      this.celular,
      this.requerimiento,
      this.logisticaInversa,
      this.descripcion,
      this.esLte,
      this.tipoCambio);
  Map<String, dynamic> toMap() {
    return {
      'paquete': paquete,
      'paqueteIdDis': paqueteIdDis,
      'secobra': secobra,
      'montoColones': montoColones,
      'montoDolares': montoDolares,
      'recibe': recibe,
      'telefono': telefono,
      'celular': celular,
      'requerimiento': requerimiento,
      'logisticaInversa': logisticaInversa,
      'descripcion': descripcion,
      'esLte': esLte,
      'tipoCambio':tipoCambio
    };
  }
}

class EntregaPaqueteState extends State<EntregaPaquete> {
  TextEditingController _controller;
  String _numeroPaquete;
  String _nombreClienteFinal;
  String _contenido;
  String _telefonoCelular;
  String _telefonoFijo;
  String _logisticaInversa;
  String _manejoEspecial;
  int _paqueteId;
  FocusNode myFocusNode;
  bool _mostrarFabMenu = false;
  bool _pagoRegistrado = false;
  double _montoPagadoDolares = 0;
  double _montoPagadoColones = 0;
  double _montoPendienteDolares = 0;
  double _montoPendienteColones = 0;
  String monedaSeleccion = 'COLONES';
  String metodoPagoSeleccion = 'EFECTIVO';
  File _image;
  final _formKey = GlobalKey<FormState>();
  final recibe_nombre = TextEditingController();
  final recibe_id = TextEditingController();
  final monto_pago = TextEditingController();
  final detalle_pago = TextEditingController();
  Paquete paqueteEntrega =
      Paquete(0, 0, "-", "-", "-", "-", "-", "-", "-", "-", "-", "-",500);

  String estadoFoto =
      "-Registra una foto para respaldar el intento de entrega-";
  int _selectedIndex = 0;
  static const TextStyle optionStyle =
      TextStyle(fontSize: 30, fontWeight: FontWeight.bold);


  void estableceNumeroPaquete(numPaquete) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('paqueteEntrega', numPaquete);
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.of(context).pop();
    } else if (index == 1) {
      
      if(paqueteEntrega.secobra == "S" && _pagoRegistrado == false) {
        registraPago();
      }else {
        procesaEntrega();
      }
      
    } else if (index == 2) {
      registraPago();
    }
    setState(() {
      _selectedIndex = index;
    });
  }

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('path_img', image.path);
    setState(() {
      _image = image;
      estadoFoto = "Foto Agregada";
    });
  }

  void initState() {
    super.initState();
    _controller = TextEditingController();
    _numeroPaquete = "0000";
    _nombreClienteFinal = "Nombre Cliente Final";
    _telefonoCelular = "888888888";
    _telefonoFijo = "2222222222";
    _contenido = "Contenido";
    _logisticaInversa = "NO";
    _manejoEspecial = "NO";
    _paqueteId = 0;
    // myFocusNode = FocusNode();
    //_controller.text = ObtieneVariable();
  }

  Widget BuildCardPaquete() {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: <Widget>[
        ListTile(
          leading: Icon(Icons.view_agenda, size: 50, color: Colors.grey[400]),
          title: Text(_numeroPaquete),
          subtitle: Text(_contenido),
        ),
        ListTile(
          leading: Icon(Icons.account_box, size: 50, color: Colors.grey[400]),
          title: Text("Persona que recibe"),
          subtitle: Text(_nombreClienteFinal),
        ),
        ListTile(
          leading: Icon(Icons.phone_android, size: 50, color: Colors.grey[400]),
          title: Text("Celular"),
          subtitle: Text(_telefonoCelular),
        ),
        ListTile(
          leading: Icon(Icons.phone, size: 50, color: Colors.grey[400]),
          title: Text("Tel. Fijo"),
          subtitle: Text(_telefonoFijo),
        ),
        ListTile(
          leading: Icon(Icons.grade, size: 50, color: Colors.grey[400]),
          title: Text("Manejo Especial"),
          subtitle: Text(_manejoEspecial),
        ),
        ListTile(
          leading:
              Icon(Icons.compare_arrows, size: 50, color: Colors.grey[400]),
          title: Text("¿Requiere logistica inversa?"),
          subtitle: Text(_logisticaInversa),
        ),
      ],
    );
  }

  Widget PantallaEntrega() {
    String num_paquete = ModalRoute.of(context).settings.arguments;
    if (num_paquete == null) {
      num_paquete = "";
    } else {
      _controller.text = num_paquete;
    }
    return SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding:
                const EdgeInsets.only(right: 8, left: 8, top: 20, bottom: 12),
            child: TextField(
              obscureText: false,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Número de Paquete',
              ),
              controller: _controller,
              onSubmitted: (term) {
                //obtienePaquete(term, context);
                obtienePaqueteLocal(term, context);
              },
              //autofocus: true,
              focusNode: myFocusNode,
              //autofocus: true,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Card(
              child: BuildCardPaquete(),
            ),
          ),
        ],
      ),
    );
  }

  Future sincronizaData() async {}

  Future registraPago() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
            title: Text(
              'Monto a cobrar:',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            content: StatefulBuilder(
                builder: (BuildContext context, StateSetter setState) {
              return Form(
                key: _formKey,
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                    SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.all(1.0),
                            child: Text("Pagar: ¢: " +
                                paqueteEntrega.montoColones +
                                " \$: " +
                                paqueteEntrega.montoDolares),
                          ),
                          
                          Padding(
                            padding: EdgeInsets.all(1.0),
                            child: Text("Falta: ¢: " +
                                _montoPendienteColones.toString()+
                                " \$: " +
                                _montoPendienteDolares.toString()),
                          ),
                          Padding(
                            padding: EdgeInsets.all(2.0),
                            child: TextFormField(
                              decoration:
                                  InputDecoration(labelText: 'Monto del pago'),
                              controller: monto_pago,
                              validator: (value) {
                                if (value.isEmpty) {
                                  return 'El monto es obligatorio';
                                }
                                return null;
                              },
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(2.0),
                            child: DropdownButton<String>(
                              value: monedaSeleccion,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  monedaSeleccion = newValue;
                                });
                              },
                              items: <String>[
                                'COLONES',
                                'DOLARES'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(2.0),
                            child: DropdownButton<String>(
                              value: metodoPagoSeleccion,
                              icon: Icon(Icons.arrow_downward),
                              iconSize: 24,
                              elevation: 16,
                              style: TextStyle(color: Colors.deepPurple),
                              underline: Container(
                                height: 2,
                                color: Colors.deepPurpleAccent,
                              ),
                              onChanged: (String newValue) {
                                setState(() {
                                  metodoPagoSeleccion = newValue;
                                });
                              },
                              items: <String>[
                                'EFECTIVO',
                                'TARJETA',
                                'DEPOSITO'
                              ].map<DropdownMenuItem<String>>((String value) {
                                return DropdownMenuItem<String>(
                                  value: value,
                                  child: Text(value),
                                );
                              }).toList(),
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.all(2.0),
                            child: TextFormField(
                              decoration: InputDecoration(
                                  labelText: 'Referencia/Detalle'),
                              controller: detalle_pago,
                              validator: (value) {
                                /*if (value.isEmpty) {
                        return 'Detalle o Referencia Obligatoria';
                      }*/
                                return null;
                              },
                            ),
                          ),
                        ],
                      ),
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: RaisedButton(
                              child: Text(
                                "Cancelar",
                                style: TextStyle(color: Colors.black),
                              ),
                              onPressed: () {
                                estableceNumeroPaquete(_controller.value.text);
                                Navigator.of(context).pop();
                              }),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(20.0),
                          child: RaisedButton(
                            color: Colors.blueAccent,
                            child: Text(
                              "Registrar Pago",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.white),
                            ),
                            onPressed: () {
                              if (_formKey.currentState.validate()) {
                                _formKey.currentState.save();
                                registrarPagoLocal(context);
                                setState(() {
                                  _montoPendienteDolares = _montoPendienteDolares;
                                  _montoPendienteColones = _montoPendienteColones;
                                });
                              }
                            },
                          ),
                        )
                      ],
                    )
                  ],
                ),
              );
            }));
      },
    );
  }

  Future<void> registrarPagoLocal(context) async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1); 
    final Database db = await database;
    //await db.execute('DROP TABLE  Pagos'); 
    await db.execute('CREATE TABLE IF NOT EXISTS Pagos (id INTEGER PRIMARY KEY AUTOINCREMENT, paqueteID INTEGER, moneda INTEGER, metodo INTEGER, monto DOUBLE, detalle TEXT)');
    int moneda = 0, metodo = 0;
    double montoPago = 0;
    if(metodoPagoSeleccion == "EFECTIVO") {
      metodo = 1;
    } else if(metodoPagoSeleccion == "TARJETA") {
      metodo = 2;
    } else if(metodoPagoSeleccion == "DEPOSITO") {
      metodo = 3;
    } 

    if(monedaSeleccion == "COLONES") {
      moneda = 1;
    }else if(monedaSeleccion == "DOLARES") {
      moneda = 2;
    }
    montoPago = double.parse(monto_pago.text);
    Pago pago = Pago(paqueteEntrega.paqueteIdDis, moneda, metodo, montoPago, detalle_pago.text);
    //await deleteDatabase(path);
    await db.insert(
      'Pagos',
      pago.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    Navigator.of(context).pop();
    detalle_pago.clear();
    monto_pago.clear();
    await calculaTotalesPaquete();
     showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('PAGO REGISTRADO'),
            content: Text("EL PAGO HA SIDO REGISTRADO"),
            actions: <Widget>[
              (_pagoRegistrado)?
              FlatButton(
                child: Text('Entregar Paquete'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                  procesaEntrega();
                },
              ):
              FlatButton(
                child: Text('OTRO PAGO'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                  registraPago();
                },
              ),
              
            ],
          );
        },
      );
    if(_pagoRegistrado) {
      Navigator.of(context).pop();
     
    }else {
      
      
    }
    
  }

  Future calculaTotalesPaquete() async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1); // Get a r
    final Database db = await database;
    await db.execute('CREATE TABLE IF NOT EXISTS Pagos (id INTEGER PRIMARY KEY AUTOINCREMENT, paqueteID INTEGER, moneda INTEGER, metodo INTEGER, monto DOUBLE, detalle TEXT)');
    final List<Map<String, dynamic>> maps = await db.query('Pagos');
    double dolaresAcolones = 0;
    double colonesAdolares = 0;
    _montoPagadoDolares = 0;
    _montoPagadoColones = 0;
    _montoPendienteDolares = 0;
    _montoPendienteColones = 0;
    List.generate(maps.length, (i) {
      if (maps[i]['paqueteID'] == paqueteEntrega.paqueteIdDis) { 
        if(maps[i]['moneda'] == 1){
          _montoPagadoColones += maps[i]['monto'];
        }else {
          _montoPagadoDolares += maps[i]['monto'];
        }
      }
    });

    //CUBIERTO EN COLONES
    dolaresAcolones = _montoPagadoDolares * paqueteEntrega.tipoCambio;
    colonesAdolares = _montoPagadoColones / paqueteEntrega.tipoCambio;
    _montoPendienteDolares = double.parse(paqueteEntrega.montoDolares) - (colonesAdolares+_montoPagadoDolares);
    _montoPendienteColones = double.parse(paqueteEntrega.montoColones) - (dolaresAcolones+_montoPagadoColones);
    _montoPendienteDolares = dp(_montoPendienteDolares ,2);
    _montoPendienteColones = dp(_montoPendienteColones ,2);
    if(_montoPendienteColones <= 0 || _montoPendienteDolares <= 0) {
      _pagoRegistrado=true;
    }
    
  }

  double dp(double val, int places){ 
   double mod = pow(10.0, places); 
   return ((val * mod).round().toDouble() / mod); 
  }
  Future procesaEntrega() {
    return showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(
            'Datos de quien recibe...',
            style: TextStyle(fontWeight: FontWeight.bold),
          ),
          content: Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(1.0),
                  child: TextFormField(
                    decoration: InputDecoration(labelText: 'Nombre completo'),
                    controller: recibe_nombre,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'El nombre es obligatorio';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(8.0),
                  child: TextFormField(
                    decoration: InputDecoration(labelText: 'Identificación'),
                    controller: recibe_id,
                    validator: (value) {
                      if (value.isEmpty) {
                        return 'La cedula es obligatoria';
                      }
                      return null;
                    },
                  ),
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: RaisedButton(
                          color: Colors.blueAccent,
                          child: Text(
                            "Firma",
                            style: TextStyle(color: Colors.white),
                          ),
                          onPressed: () {
                            estableceNumeroPaquete(_controller.value.text);
                            Navigator.of(context).push(
                                // With MaterialPageRoute, you can pass data between pages,
                                // but if you have a more complex app, you will quickly get lost.
                                MaterialPageRoute(
                              builder: (context) => SignApp(),
                            ));
                          }),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: RaisedButton(
                        child: Text(
                          "Entregar",
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        onPressed: () {
                          if (_formKey.currentState.validate()) {
                            _formKey.currentState.save();
                            validaFirma();
                          }
                        },
                      ),
                    )
                  ],
                )
              ],
            ),
          ),
        );
      },
    );
  }

  Future validaFirma() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String firma = prefs.getString('firmaCliente');
    if (firma == null) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('¡Falta la firma!'),
            content: Text("El cliente debe de firmar"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    } else {
      guardaEntrega("EN", context);
    }
  }

  Future<bool> hayconexion() async{
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        return true;
      }
    } on SocketException catch (_) {
     return false;
    }
    return false;
  }

  Future obtienePaqueteLocal(paquete, contex) async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = await database;
    int paqueteId = int.parse(paquete);
      setState(() {
       
        _image = null;
        _mostrarFabMenu = false;
        paqueteEntrega.celular = "";
        paqueteEntrega.descripcion = "";
        paqueteEntrega.esLte = "N";
        paqueteEntrega.paqueteIdDis = 0;
        paqueteEntrega.paquete = 0;
        paqueteEntrega.montoDolares = "";
        paqueteEntrega.montoColones = "";
        paqueteEntrega.logisticaInversa = "";
        paqueteEntrega.requerimiento = "";
        paqueteEntrega.tipoCambio = 500;
        _pagoRegistrado = false;
        monedaSeleccion = 'COLONES';
        metodoPagoSeleccion = 'EFECTIVO';
      });

    //final List<Map<String, dynamic>> maps1 = await db.rawQuery('SELECT * FROM Paquetes WHERE paquete='+paquete);
    final List<Map<String, dynamic>> maps = await db.query('Paquetes');
    List.generate(maps.length, (i) {
      if (maps[i]['paquete'] == paqueteId) {
        String descripcion = "NO HAY";
        if(maps[i]['descripcion'] != null) {
          descripcion = maps[i]['descripcion'];
        }
        paqueteEntrega.paquete = maps[i]['paquete'];
        paqueteEntrega.paqueteIdDis = maps[i]['paqueteIdDis'];
        paqueteEntrega.secobra = maps[i]['secobra'];
        paqueteEntrega.montoColones = maps[i]['montoColones'];
        paqueteEntrega.montoDolares = maps[i]['montoDolares'];
        paqueteEntrega.recibe = maps[i]['recibe'];
        paqueteEntrega.telefono = maps[i]['telefono'];
        paqueteEntrega.celular = maps[i]['celular'];
        paqueteEntrega.descripcion = descripcion;
        paqueteEntrega.requerimiento = maps[i]['requerimiento'];
        paqueteEntrega.logisticaInversa = maps[i]['logisticaInversa'];
        paqueteEntrega.esLte = maps[i]['esLte'];
        paqueteEntrega.tipoCambio = maps[i]['tipoCambio'];
      }
    });
    if (paqueteEntrega.paquete > 0) {
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('paquete', _controller.value.text);
      prefs.setString('firmaCliente', null);
      setState(() {
        _numeroPaquete = _controller.value.text;
        _nombreClienteFinal = paqueteEntrega.recibe;
        _telefonoCelular = paqueteEntrega.telefono;
        _paqueteId = paqueteEntrega.paquete;
        _contenido = paqueteEntrega.descripcion;
        _logisticaInversa = paqueteEntrega.logisticaInversa;
        _manejoEspecial = paqueteEntrega.requerimiento;
        _paqueteId = paqueteEntrega.paqueteIdDis;
        _mostrarFabMenu = true;
        estadoFoto = "-Registra una foto para respaldar el intento de entrega-";
        _mostrarFabMenu = true;
        estadoFoto = "-Registra una foto para respaldar el intento de entrega-";
      });
      calculaTotalesPaquete();
    } else {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('¡Error!'),
            content: Text("PAQUETE NO REGISTRADO PARA ENTREGA"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ok'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        },
      );
    }
  }

  Future guardaImagen(bitacora_id, estado) async {
    Map<String, dynamic> responseData;
    http.Response response;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String base64Image = "";
    if (estado == "EN") {
      base64Image = prefs.getString('firmaCliente');
    } else {
      List<int> imageBytes = _image.readAsBytesSync();
      base64Image = base64Encode(imageBytes);
    }
    //"R0lGODlhPQBEAPeoAJosM//AwO/AwHVYZ/z595kzAP/s7P+goOXMv8+fhw/v739/f+8PD98fH/8mJl+fn/9ZWb8/PzWlwv///6wWGbImAPgTEMImIN9gUFCEm/gDALULDN8PAD6atYdCTX9gUNKlj8wZAKUsAOzZz+UMAOsJAP/Z2ccMDA8PD/95eX5NWvsJCOVNQPtfX/8zM8+QePLl38MGBr8JCP+zs9myn/8GBqwpAP/GxgwJCPny78lzYLgjAJ8vAP9fX/+MjMUcAN8zM/9wcM8ZGcATEL+QePdZWf/29uc/P9cmJu9MTDImIN+/r7+/vz8/P8VNQGNugV8AAF9fX8swMNgTAFlDOICAgPNSUnNWSMQ5MBAQEJE3QPIGAM9AQMqGcG9vb6MhJsEdGM8vLx8fH98AANIWAMuQeL8fABkTEPPQ0OM5OSYdGFl5jo+Pj/+pqcsTE78wMFNGQLYmID4dGPvd3UBAQJmTkP+8vH9QUK+vr8ZWSHpzcJMmILdwcLOGcHRQUHxwcK9PT9DQ0O/v70w5MLypoG8wKOuwsP/g4P/Q0IcwKEswKMl8aJ9fX2xjdOtGRs/Pz+Dg4GImIP8gIH0sKEAwKKmTiKZ8aB/f39Wsl+LFt8dgUE9PT5x5aHBwcP+AgP+WltdgYMyZfyywz78AAAAAAAD///8AAP9mZv///wAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACH5BAEAAKgALAAAAAA9AEQAAAj/AFEJHEiwoMGDCBMqXMiwocAbBww4nEhxoYkUpzJGrMixogkfGUNqlNixJEIDB0SqHGmyJSojM1bKZOmyop0gM3Oe2liTISKMOoPy7GnwY9CjIYcSRYm0aVKSLmE6nfq05QycVLPuhDrxBlCtYJUqNAq2bNWEBj6ZXRuyxZyDRtqwnXvkhACDV+euTeJm1Ki7A73qNWtFiF+/gA95Gly2CJLDhwEHMOUAAuOpLYDEgBxZ4GRTlC1fDnpkM+fOqD6DDj1aZpITp0dtGCDhr+fVuCu3zlg49ijaokTZTo27uG7Gjn2P+hI8+PDPERoUB318bWbfAJ5sUNFcuGRTYUqV/3ogfXp1rWlMc6awJjiAAd2fm4ogXjz56aypOoIde4OE5u/F9x199dlXnnGiHZWEYbGpsAEA3QXYnHwEFliKAgswgJ8LPeiUXGwedCAKABACCN+EA1pYIIYaFlcDhytd51sGAJbo3onOpajiihlO92KHGaUXGwWjUBChjSPiWJuOO/LYIm4v1tXfE6J4gCSJEZ7YgRYUNrkji9P55sF/ogxw5ZkSqIDaZBV6aSGYq/lGZplndkckZ98xoICbTcIJGQAZcNmdmUc210hs35nCyJ58fgmIKX5RQGOZowxaZwYA+JaoKQwswGijBV4C6SiTUmpphMspJx9unX4KaimjDv9aaXOEBteBqmuuxgEHoLX6Kqx+yXqqBANsgCtit4FWQAEkrNbpq7HSOmtwag5w57GrmlJBASEU18ADjUYb3ADTinIttsgSB1oJFfA63bduimuqKB1keqwUhoCSK374wbujvOSu4QG6UvxBRydcpKsav++Ca6G8A6Pr1x2kVMyHwsVxUALDq/krnrhPSOzXG1lUTIoffqGR7Goi2MAxbv6O2kEG56I7CSlRsEFKFVyovDJoIRTg7sugNRDGqCJzJgcKE0ywc0ELm6KBCCJo8DIPFeCWNGcyqNFE06ToAfV0HBRgxsvLThHn1oddQMrXj5DyAQgjEHSAJMWZwS3HPxT/QMbabI/iBCliMLEJKX2EEkomBAUCxRi42VDADxyTYDVogV+wSChqmKxEKCDAYFDFj4OmwbY7bDGdBhtrnTQYOigeChUmc1K3QTnAUfEgGFgAWt88hKA6aCRIXhxnQ1yg3BCayK44EWdkUQcBByEQChFXfCB776aQsG0BIlQgQgE8qO26X1h8cEUep8ngRBnOy74E9QgRgEAC8SvOfQkh7FDBDmS43PmGoIiKUUEGkMEC/PJHgxw0xH74yx/3XnaYRJgMB8obxQW6kL9QYEJ0FIFgByfIL7/IQAlvQwEpnAC7DtLNJCKUoO/w45c44GwCXiAFB/OXAATQryUxdN4LfFiwgjCNYg+kYMIEFkCKDs6PKAIJouyGWMS1FSKJOMRB/BoIxYJIUXFUxNwoIkEKPAgCBZSQHQ1A2EWDfDEUVLyADj5AChSIQW6gu10bE/JG2VnCZGfo4R4d0sdQoBAHhPjhIB94v/wRoRKQWGRHgrhGSQJxCS+0pCZbEhAAOw==";
    //String paqueteId = _paqueteId.toString();
    //final SharedPreferences prefs = await SharedPreferences.getInstance();
    Documento docEnviar = new Documento(_paqueteId, bitacora_id, base64Image);
    String json = jsonEncode(docEnviar);

    //String authData = "pq_id=$paqueteId&btr_id=$bitacora_id&documento=$base64Image";
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/GuardaDocumento/',
            body: json,
            headers: {'Content-Type': 'application/json'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );
      if (response.statusCode == 200) {
        //final Map<String, dynamic> responseData = json.decode(response.body);
        prefs.setString('firmaCliente', null);
      }
    } catch (error) {
      var message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': true, 'message': message});
      print(message);
    }
  }

  Future<void> guardaEntrega(String estado, context) async {
    var databasesPath = await getDatabasesPath();
    String recibido_nombre_p = recibe_nombre.value.text.toUpperCase();
    String recibido_id_p = recibe_id.value.text.toUpperCase();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    int idPaquete = paqueteEntrega.paqueteIdDis;
    Database database = await openDatabase(path, version: 1); // Get a reference to the database.
    final Database db = await database;
    await db.execute('CREATE TABLE IF NOT EXISTS Entrega (id INTEGER PRIMARY KEY, numeroPaquete INTEGER, imagenPath TEXT, estado TEXT, sincronizado TEXT, recibidoNombre TEXT, recibidoId TEXT)');
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String img_path = prefs.getString('path_img');
    Entrega entrega =
        Entrega(paqueteEntrega.paqueteIdDis, paqueteEntrega.paquete, img_path, estado, 'N', recibido_nombre_p, recibido_id_p);

    await db.insert(
      'Entrega',
      entrega.toMap(),
      conflictAlgorithm: ConflictAlgorithm.replace,
    );

    bool conexion = await hayconexion();
    if(conexion) {
      await actualizaEstado(estado, context);
      await db.rawQuery("UPDATE Entrega SET sincronizado = 'S' WHERE id = $idPaquete");
    }else {

        _controller.clear();
        recibe_id.clear();
        recibe_nombre.clear();
        detalle_pago.clear();
        monto_pago.clear();
        prefs.setString("paquete", "");
        prefs.setString('firmaCliente', null);
       
        setState(() {
          _controller = TextEditingController();
          _numeroPaquete = "0000";
          _nombreClienteFinal = "Nombre Cliente Final";
          _telefonoCelular = "888888888";
          _telefonoFijo = "2222222222";
          _contenido = "Contenido";
          _logisticaInversa = "NO";
          _manejoEspecial = "NO";
          _paqueteId = 0;
          _image = null;
          _mostrarFabMenu = false;
          _montoPagadoColones = 0;
          _montoPagadoDolares = 0;
          _montoPendienteColones = 0;
          _montoPendienteDolares = 0;
          paqueteEntrega.celular = "";
          paqueteEntrega.descripcion = "";
          paqueteEntrega.esLte = "N";
          paqueteEntrega.paqueteIdDis = 0;
          paqueteEntrega.paquete = 0;
          paqueteEntrega.montoDolares = "";
          paqueteEntrega.montoColones = "";
          paqueteEntrega.logisticaInversa = "";
          paqueteEntrega.requerimiento = "";
          _pagoRegistrado = false;
          monedaSeleccion = 'COLONES';
          metodoPagoSeleccion = 'EFECTIVO';
        });
      Navigator.of(context).pop();
      showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('REALIZADA - SIN CONEXIÓN'),
                content: Text('EL PAQUETE SE PROCESÓ CON ÉXITO PERO DEBIDO A PROBLEMAS CON LA CONEXIÓN NO SE SINCRONIZÓ CON LA BASE DE DATOS'),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  )
                ],
              );
            },
          );
    }

  }

  Future actualizaEstado(estado, context) async {
    http.Response response;
    bool hasError = true;
    Map<String, dynamic> responseData;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String paquete = paqueteEntrega.paqueteIdDis.toString();
    String recibido_nombre_p = recibe_nombre.value.text.toUpperCase();
    String recibido_id_p = recibe_id.value.text.toUpperCase();
    String authData = "estado_p=$estado&paquete_p=$paquete&recibido_nombre_p=$recibido_nombre_p&recibido_id_p=$recibido_id_p";
    String message = 'Ha ocurrido un error.';
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/EntregaPaquete/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);

        if (_image != null || estado == "EN") {
          await guardaImagen(responseData['BITACORA_ID'], estado);
        }
        
        await sincronizaPagos();
        if(paqueteEntrega.esLte == "S" && estado == "EN") {
          await actualizaEstadoLTE("18");
        }else  if(paqueteEntrega.esLte == "S") {
          await actualizaEstadoLTE("19");
        }
        _controller.clear();
        recibe_id.clear();
        recibe_nombre.clear();
        detalle_pago.clear();
        monto_pago.clear();
        prefs.setString("paquete", "");
        prefs.setString('firmaCliente', null);
       
        setState(() {
          _controller = TextEditingController();
          _numeroPaquete = "0000";
          _nombreClienteFinal = "Nombre Cliente Final";
          _telefonoCelular = "888888888";
          _telefonoFijo = "2222222222";
          _contenido = "Contenido";
          _logisticaInversa = "NO";
          _manejoEspecial = "NO";
          _paqueteId = 0;
          _image = null;
          _mostrarFabMenu = false;
          paqueteEntrega.celular = "";
          paqueteEntrega.descripcion = "";
          paqueteEntrega.esLte = "N";
          paqueteEntrega.paqueteIdDis = 0;
          paqueteEntrega.paquete = 0;
          paqueteEntrega.montoDolares = "";
          paqueteEntrega.montoColones = "";
          paqueteEntrega.logisticaInversa = "";
          paqueteEntrega.requerimiento = "";
          _pagoRegistrado = false;
          monedaSeleccion = 'COLONES';
          metodoPagoSeleccion = 'EFECTIVO';
        });

        if (estado != "EN") {
          Navigator.of(context, rootNavigator: true).pop('dialog');
        } else {
          showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('¡Entregado con exito!'),
                content: Text("Se ha registrado la entrega del paquete"),
                actions: <Widget>[
                  FlatButton(
                    child: Text('Ok'),
                    color: Theme.of(context).buttonColor,
                    onPressed: () {
                      Navigator.of(context).pop();
                      Navigator.of(context, rootNavigator: true).pop('dialog');
                    },
                  )
                ],
              );
            },
          );
        }
      } else {
        throw Exception('Fallo al consultar WS');
      }
    } catch (error) {
      message = "Error al conectar con el servidor, Error: " + error;
      responseData.addAll({'success': !hasError, 'message': message});
      print(message);
    }
  }

  Future<bool> sincronizaPagos() async {
    var databasesPath = await getDatabasesPath();
    String path = ph.join(await databasesPath, 'entregas_offline.db');
    Database database = await openDatabase(path, version: 1);
    final Database db = await database;
    //final List<Map<String, dynamic>> maps1 = await db.rawQuery('SELECT * FROM Paquetes WHERE paquete='+paquete);
    final List<Map<String, dynamic>> maps = await db.query('Pagos');
    List.generate(maps.length, (i) async {
      if (maps[i]['paqueteID'] == paqueteEntrega.paqueteIdDis) {
        Pago pago = Pago(maps[i]['paqueteID'],maps[i]['moneda'], maps[i]['metodo'], maps[i]['monto'], maps[i]['detalle']);
        await insertaPagoBD(pago);
      }
    });
    return true;
  }

  Future<bool> insertaPagoBD(Pago pago) async {
    http.Response response;
    int paqueteID = pago.paqueteID;
    int moneda = pago.moneda;
    double monto = pago.monto;
    int metodo = pago.metodo;
    String concepto = pago.detalle;
    String authData = "paquete_id_p=$paqueteID&moneda_p=$moneda&monto_p=$monto&metodo_p=$metodo&concepto_p=$concepto";
    try {
      response = await http
          .post(
            'http://192.168.6.180:8080/devrenoir/tsmwww/lte/InsertaPago/',
            body: authData,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 40),
          );

      if (response.statusCode == 200) {
        return true;
      }
    }catch (error) {
      return false;
    }
    return true;
  }

  Future<bool> actualizaEstadoLTE(String estado) async {
    http.Response response;
    String pusuario = "LTEMIAMI";
    String pclave = "RtXlN8cXOxSMy8YI4gL8qNuE3_5Yp4kUm4wYQmKFgiq5wPzdSV";
     Map<String,String> headers = {
      'Content-type' : 'application/json', 
      'Accept': 'application/json',
    };
    //String authData = "pusuario=$pusuario&pclave=$pclave&ppaquete=$ppaquete";
    var body = json.encode({"pusuario": pusuario, "pclave": pclave, "ppaquete": paqueteEntrega.paquete, "pestado":estado});
    try {
      response = await http
          .post(
            'http://200.74.255.35:8080/ords/xpe/miami/cambiaestadopaquete/',
            body: body,
            headers: headers
            //encoding: Encoding.getByName("utf-8"),
          )
          .timeout(
            Duration(seconds: 10),
          );

      if (response.statusCode == 200) {
        var responseLTE = json.decode(response.body);
        //var responseResultado = responseLTE["RESULTADO"];
        if(responseLTE["RESULTADO"]["ERROR"] == "S") {
          
        }
      } else {
        throw Exception('Fallo al consultar WS');

      }
    } catch (error) {
      return false;
    }
    return false;
  }

  Future muestraModal(estado) {
    String title = "";
    if (estado == "IE") {
      title = "Intento de Entrega";
    }

    if (estado == "NE") {
      title = "El cliente no se encuentra";
    }

    if (estado == "NP") {
      title = "El cliente no paga";
    }

    if (estado == "NQ") {
      title = "Paquete Rechazado";
    }

    if (estado == "DI") {
      title = "Dirección Incorrecta";
    }
    return showDialog(
      context: context,
      builder: (context) {
        String btnText = "Actualizar";
        return StatefulBuilder(
          builder: (context, setState) {
            return AlertDialog(
              title: Text(title),
              content: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Text("¿Seguro que desea actualizar el estado del paquete?"),
                  Padding(
                    padding: EdgeInsets.only(top: 16.0, bottom: 16.0),
                    child: Text(estadoFoto,
                        style: TextStyle(fontStyle: FontStyle.italic)),
                  ),
                  FlatButton(
                    child: Icon(Icons.camera_alt),
                    color: Colors.red,
                    onPressed: () {
                      getImage();
                    },
                  ),
                ],
              ),
              actions: <Widget>[
                FlatButton(
                  child: Text('Cancelar'),
                  color: Theme.of(context).buttonColor,
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
                FlatButton(
                  child: Text(btnText),
                  color: Colors.blueAccent,
                  onPressed: () {
                    guardaEntrega(estado, context);
                    //actualizaEstado(estado, context);
                    setState(() {
                      btnText = "Actualizando...";
                    });
                  },
                )
              ],
            );
          },
        );
      },
    );
  }

  Widget build(BuildContext context) {
    final key = new GlobalKey<ScaffoldState>();
    return Scaffold(
      appBar: AppBar(
        title: Text("Entregar Paquete"),
      ),
      key: key,
      drawer: MenuPrincipal(),
      body: PantallaEntrega(),
      floatingActionButton: (_mostrarFabMenu)
          ? BoomMenu(
              animatedIcon: AnimatedIcons.menu_close,
              animatedIconTheme: IconThemeData(size: 22.0),
              //child: Icon(Icons.add),
              onOpen: () => print('OPENING DIAL'),
              onClose: () => print('DIAL CLOSED'),
              overlayColor: Colors.black,
              overlayOpacity: 0.7,
              children: [
                MenuItem(
                    child: Icon(Icons.assignment_late, color: Colors.black),
                    title: "Intento de entrega",
                    titleColor: Colors.white,
                    subtitle: "Registrar intento de entrega",
                    subTitleColor: Colors.white,
                    backgroundColor: Colors.deepOrangeAccent,
                    onTap: () => muestraModal("IE")),
                MenuItem(
                  child: Icon(Icons.error, color: Colors.black),
                  title: "No está",
                  titleColor: Colors.white,
                  subtitle: "El cliente no se encuentra",
                  subTitleColor: Colors.white,
                  backgroundColor: Colors.grey,
                  onTap: () => muestraModal("NE"),
                ),
                MenuItem(
                  child: Icon(Icons.money_off, color: Colors.black),
                  title: "No paga",
                  titleColor: Colors.white,
                  subtitle: "El cliente se niega a pagar",
                  subTitleColor: Colors.white,
                  backgroundColor: Colors.grey,
                  onTap: () => muestraModal("NP"),
                ),
                MenuItem(
                  child: Icon(Icons.record_voice_over, color: Colors.black),
                  title: "No quiso recibir",
                  titleColor: Colors.white,
                  subtitle: "El cliente ha rechazado el paquete",
                  subTitleColor: Colors.white,
                  backgroundColor: Colors.grey,
                  onTap: () => muestraModal("NQ"),
                ),
                MenuItem(
                  child: Icon(Icons.location_off, color: Colors.black),
                  title: "Dirección Incorrecta",
                  titleColor: Colors.white,
                  subtitle: "La dirección no es correcta",
                  subTitleColor: Colors.white,
                  backgroundColor: Colors.grey,
                  onTap: () => muestraModal("DI"),
                ),
              ],
            )
          : null,
      bottomNavigationBar: (_mostrarFabMenu)
          ? BottomNavigationBar(
              backgroundColor: Colors.white,
              items: <BottomNavigationBarItem>[
                BottomNavigationBarItem(
                  icon: Icon(Icons.arrow_back),
                  title: Text('Regresar'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.flag),
                  title: Text('Entregar'),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.monetization_on),
                  title: Text('Pago'),
                )
              ],
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.red[800],
              onTap: _onItemTapped,
            )
          : null,
    );
  }
}

class SignApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SignAppState();
  }
}

class SignAppState extends State<SignApp> {
  GlobalKey<SignatureState> signatureKey = GlobalKey();
  var image;
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    initPlatformState();
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
    print(_platformVersion);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Signature(key: signatureKey),
      persistentFooterButtons: <Widget>[
        FlatButton(
          child: Text('Limpiar'),
          onPressed: () {
            signatureKey.currentState.clearPoints();
          },
        ),
        FlatButton(
          child: Text('Guardar'),
          onPressed: () {
            // Future will resolve later
            // so setState @image here and access in #showImage
            // to avoid @null Checks
            setRenderedImage(context);
          },
        )
      ],
    );
  }

  setRenderedImage(BuildContext context) async {
    ui.Image renderedImage = await signatureKey.currentState.rendered;

    setState(() {
      image = renderedImage;
    });

    generateBase64(context);
  }

  Future<Null> generateBase64(BuildContext context) async {
    var pngBytes = await image.toByteData(format: ui.ImageByteFormat.png);

    final permissionValidator = EasyPermissionValidator(
      context: context,
      appName: 'Easy Permission Validator',
    );
    var result = await permissionValidator.storage();
    if (result) {
      // Use plugin [path_provider] to export image to storage
      Directory directory = await getExternalStorageDirectory();
      String path = directory.path;
      await Directory('$path/$directoryName').create(recursive: true);
      var fecha = formattedDate();
      await File('$path/$directoryName/${fecha}.png')
          .writeAsBytesSync(pngBytes.buffer.asInt8List());
      List<int> imageBytes =
          File('$path/$directoryName/${fecha}.png').readAsBytesSync();
      //  String firma = pngBytes.toString();
      var firmabsae = base64Encode(imageBytes);
      final SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs.setString('firmaCliente', firmabsae);
      prefs.setString('path_img', '$path/$directoryName/${fecha}.png');
      Navigator.pop(context);
    }
  }

  String formattedDate() {
    DateTime dateTime = DateTime.now();
    String dateTimeString = 'Signature_' +
        dateTime.year.toString() +
        dateTime.month.toString() +
        dateTime.day.toString() +
        dateTime.hour.toString() +
        ':' +
        dateTime.minute.toString() +
        ':' +
        dateTime.second.toString() +
        ':' +
        dateTime.millisecond.toString() +
        ':' +
        dateTime.microsecond.toString();
    return dateTimeString;
  }
}

class Signature extends StatefulWidget {
  Signature({Key key}) : super(key: key);
  @override
  State<StatefulWidget> createState() {
    return SignatureState();
  }
}

class SignatureState extends State<Signature> {
  // [SignatureState] responsible for receives drag/touch events by draw/user
  // @_points stores the path drawn which is passed to
  // [SignaturePainter]#contructor to draw canvas
  List<Offset> _points = <Offset>[];

  Future<ui.Image> get rendered {
    // [CustomPainter] has its own @canvas to pass our
    // [ui.PictureRecorder] object must be passed to [Canvas]#contructor
    // to capture the Image. This way we can pass @recorder to [Canvas]#contructor
    // using @painter[SignaturePainter] we can call [SignaturePainter]#paint
    // with the our newly created @canvas
    ui.PictureRecorder recorder = ui.PictureRecorder();
    Canvas canvas = Canvas(recorder);
    SignaturePainter painter = SignaturePainter(points: _points);
    var size = context.size;
    painter.paint(canvas, size);
    return recorder
        .endRecording()
        .toImage(size.width.floor(), size.height.floor());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: GestureDetector(
          onPanUpdate: (DragUpdateDetails details) {
            setState(() {
              RenderBox _object = context.findRenderObject();
              Offset _locationPoints =
                  _object.localToGlobal(details.globalPosition);
              _points = new List.from(_points)..add(_locationPoints);
            });
          },
          onPanEnd: (DragEndDetails details) {
            setState(() {
              _points.add(null);
            });
          },
          child: CustomPaint(
            painter: SignaturePainter(points: _points),
            size: Size.infinite,
          ),
        ),
      ),
    );
  }

  // clearPoints method used to reset the canvas
  // method can be called using
  //   key.currentState.clearPoints();
  void clearPoints() {
    setState(() {
      _points.clear();
    });
  }
}

class SignaturePainter extends CustomPainter {
  // [SignaturePainter] receives points through constructor
  // @points holds the drawn path in the form (x,y) offset;
  // This class responsible for drawing only
  // It won't receive any drag/touch events by draw/user.
  List<Offset> points = <Offset>[];

  SignaturePainter({this.points});
  @override
  void paint(Canvas canvas, Size size) {
    var paint = Paint()
      ..color = Colors.black
      ..strokeCap = StrokeCap.square
      ..strokeWidth = 5.0;

    for (int i = 0; i < points.length - 1; i++) {
      if (points[i] != null && points[i + 1] != null) {
        canvas.drawLine(points[i], points[i + 1], paint);
      }
    }
  }

  @override
  bool shouldRepaint(SignaturePainter oldDelegate) {
    return oldDelegate.points != points;
  }
}

// *  *  *  Metodo de ejemplo  *  *  *
// class SecondPage extends StatelessWidget {
//   // This is a String for the sake of an example.
//   // You can use any type you want.
//   final String data;

//   SecondPage({
//     Key key,
//     @required this.data,
//   }) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text('Routing App'),
//       ),
//       drawer: MenuPrincipal(),
//       body: Center(
//         child: Column(
//           mainAxisSize: MainAxisSize.min,
//           children: <Widget>[
//             Text(
//               'Second Page',
//               style: TextStyle(fontSize: 50),
//             ),
//             Text(
//               data,
//               style: TextStyle(fontSize: 20),
//             ),
//           ],
//         ),
//       ),
//     );
//   }
// }
