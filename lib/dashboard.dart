import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:paqueteslte/avanceDiario.dart';
import 'package:paqueteslte/cierreDeRuta.dart';
import 'package:paqueteslte/main.dart';
import 'package:paqueteslte/menuPrincipal.dart';
import 'package:paqueteslte/listaDespachos.dart';
import 'package:paqueteslte/listaRecolectas.dart';
import 'entregaOffline.dart';
class DashBoard extends StatefulWidget {
  @override
  
  DashBoardState createState() {
    return new DashBoardState();
  }
  //createState() => new MenuPrincipalState();
}

class DashBoardState extends State<DashBoard> {
  FirebaseMessaging _firebaseMessaging = FirebaseMessaging();

 void initState() {
    super.initState();
    _firebaseMessaging.getToken().then((token){
    print(token);
  });

  _firebaseMessaging.configure(
    onMessage: (Map<String, dynamic> message) async {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('¡Nueva recolecta asignada!'),
            content: Text("Te han asignado una nueva recolecta"),
            actions: <Widget>[
              FlatButton(
                child: Text('Ver'),
                color: Theme.of(context).buttonColor,
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                  builder: (context) => RecolectasAsignadasChofer(),
                ),
                  );
                },
              )
            ],
          );
        },
      );
     // print('on message $message');
    },
    onResume: (Map<String, dynamic> message) async {
      print('on resume $message');
    },
    onLaunch: (Map<String, dynamic> message) async {
      print('on launch $message');
    },
    
  );

  }
  Widget Dashboard() {
    return Container(
      alignment: Alignment.center,
      child: ListView(
        padding: const EdgeInsets.only(top: 20.0),
        children: <Widget>[
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.local_shipping,
                  color: Colors.red[300], size: 50.0),
              title: Text('MIS RECOLECTAS',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Recolectas Asignadas',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => RecolectasAsignadasChofer(),
                  ),
                );
              },
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.airport_shuttle,
                  color: Colors.red[300], size: 50.0),
              title: Text('MIS DESPACHOS',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Despachos asignados',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => DespachosAsignadosChofer(),
                  ),
                );
              },
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.person, color: Colors.red[300], size: 50.0),
              title: Text('ENTREGAR PAQUETES',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Recolectar paquetes en ruta al CEDI',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => EntregaPaquete(),
                  ),
                );
              },
            ),
          ),
         
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.departure_board, color: Colors.red[300], size: 50.0),
              title: Text('AVANCE DE ENTREGAS',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Revisa el progreso de tu ruta',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => avanceDiario(),
                  ),
                );
              },
            ),
          ),
          Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.cancel, color: Colors.red[300], size: 50.0),
              title: Text('CIERRE DE RUTA',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Cuando completes la ruta, repotalo aquí.',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => cierreDeRuta(),
                  ),
                );
              },
            ),
          ),
           Card(
            elevation: 8.0,
            margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
            color: Colors.grey[300],
            child: ListTile(
              leading: Icon(Icons.sync, color: Colors.grey[500], size: 50.0),
              title: Text('SINCRONIZAR INFORMACIÓN',
                  style: TextStyle(
                      fontWeight: FontWeight.bold, color: Colors.black)),
              subtitle: Text('Sincroniza todas las entregas realizadas sin conexión',
                  style: TextStyle(color: Colors.grey)),
              isThreeLine: true,
              onTap: () {
                Navigator.of(context).push(
                  // With MaterialPageRoute, you can pass data between pages,
                  // but if you have a more complex app, you will quickly get lost.
                  MaterialPageRoute(
                    builder: (context) => EntregaPaqueteOffline(),
                  ),
                );
              },
            ),
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Panel de administración"),
        /* actions: <Widget>[
                IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () {
                  },
                  tooltip: 'Salir',
                )
              ],*/
      ),
      drawer: MenuPrincipal(),
      body: Dashboard(),
    );
  }
}
